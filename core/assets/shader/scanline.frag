#ifdef GL_ES
    precision mediump float;
#endif

varying vec4 vColor;
varying vec2 vTexCoord;

uniform sampler2D u_texture;
uniform mat4 u_projTrans;

const float SCANLINE_SOFTNESS = 0.7; // lower is stronger, probably should call this softness
const float SCANLINE_DENSITY = 2.0; // every 2nd pixel

void main() {

    vec4 texColor = texture2D(u_texture, vTexCoord);

    vec4 retColor = vColor;

    if(mod(gl_FragCoord.y, SCANLINE_DENSITY) < 1.0) {
        retColor.rgb *= SCANLINE_SOFTNESS;
    }

    gl_FragColor = texColor * retColor;
}