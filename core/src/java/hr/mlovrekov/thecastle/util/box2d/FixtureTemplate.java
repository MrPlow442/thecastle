/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.box2d;

import com.badlogic.gdx.physics.box2d.FixtureDef;

public class FixtureTemplate implements Buildable<FixtureDef> {

    public ShapeTemplate shape;
    public float friction = 0.2f;
    public float restitution = 0;
    public float density = 0;
    public boolean isSensor = false;
    public final FilterTemplate filter = new FilterTemplate();
    public UserData userData = new UserData();

    @Override
    public FixtureDef build() {
        FixtureDef fixtureDef = new FixtureDef();

        fixtureDef.shape = shape.build();
        fixtureDef.friction = friction;
        fixtureDef.restitution = restitution;
        fixtureDef.density = density;
        fixtureDef.isSensor = isSensor;
        filter.copyTo(fixtureDef.filter);

        return fixtureDef;
    }
}
