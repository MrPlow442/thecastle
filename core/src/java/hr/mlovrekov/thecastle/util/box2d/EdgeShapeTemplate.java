/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.box2d;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Shape;

public class EdgeShapeTemplate extends ShapeTemplate {

    private float v1X, v1Y, v2X, v2Y;
    private Float v0X, v0Y, v3X, v3Y;
    private boolean hasVertex0, hasVertex3;

    public void set (Vector2 v1, Vector2 v2) {
        set(v1.x, v1.y, v2.x, v2.y);
    }

    public void set (float v1X, float v1Y, float v2X, float v2Y) {
        this.v1X = v1X;
        this.v1Y = v1Y;
        this.v2X = v2X;
        this.v2Y = v2Y;
    }


    public void getVertex0 (Vector2 vec) {
        if(v0X != null && v0Y != null ) {
            vec.x = v0X;
            vec.y = v0Y;
        }
    }

    public void setVertex0 (Vector2 vec) {
        setVertex0(vec.x, vec.y);
    }

    public void setVertex0 (float x, float y) {
        v0X = x;
        v0Y = y;
    }


    public void getVertex3 (Vector2 vec) {
        if(v3X != null && v3Y != null ) {
            vec.x = v3X;
            vec.y = v3Y;
        }
    }

    public void setVertex3 (Vector2 vec) {
        setVertex3(vec.x, vec.y);
    }

    public void setVertex3 (float x, float y) {
        v3X = x;
        v3Y = y;
    }

    public boolean hasVertex0() {
        return hasVertex0;
    }

    public void setHasVertex0 (boolean hasVertex0) {
        this.hasVertex0 = hasVertex0;
    }

    public boolean hasVertex3 () {
        return hasVertex3;
    }

    public void setHasVertex3 (boolean hasVertex3) {
        this.hasVertex3 = hasVertex3;
    }

    @Override
    public Shape.Type getType() {
        return Shape.Type.Edge;
    }

    @Override
    public Shape build() {
        EdgeShape edgeShape = new EdgeShape();

        edgeShape.set(v1X, v1Y, v2X, v2Y);

        if(hasVertex0) {
            edgeShape.setHasVertex0(true);
            edgeShape.setVertex0(v0X, v0Y);
        }

        if(hasVertex3) {
            edgeShape.setHasVertex3(true);
            edgeShape.setVertex3(v3X, v3Y);
        }

        return edgeShape;
    }
}
