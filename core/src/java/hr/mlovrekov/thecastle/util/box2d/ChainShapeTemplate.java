/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.box2d;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Shape;

public class ChainShapeTemplate extends ShapeTemplate {

    private float[] vertices;
    private boolean isLoop;
    private Float prevVertexX, prevVertexY;
    private Float nextVertexX, nextVertexY;

    public void createLoop (float[] vertices) {
        this.vertices = vertices;
        this.isLoop = true;
    }

    public void createLoop (Vector2[] vertices) {
        float[] verts = new float[vertices.length * 2];
        for (int i = 0, j = 0; i < vertices.length * 2; i += 2, j++) {
            verts[i] = vertices[j].x;
            verts[i + 1] = vertices[j].y;
        }
        createLoop(verts);
    }

    public void createChain (float[] vertices) {
        this.vertices = vertices;
        this.isLoop = false;
    }

    public void createChain (Vector2[] vertices) {
        float[] verts = new float[vertices.length * 2];
        for (int i = 0, j = 0; i < vertices.length * 2; i += 2, j++) {
            verts[i] = vertices[j].x;
            verts[i + 1] = vertices[j].y;
        }
        createChain(verts);
    }

    public void setPrevVertex (Vector2 prevVertex) {
        setPrevVertex(prevVertex.x, prevVertex.y);
    }

    public void setPrevVertex (float prevVertexX, float prevVertexY) {
        this.prevVertexX = prevVertexX;
        this.prevVertexY = prevVertexY;
    }

    public void setNextVertex (Vector2 nextVertex) {
        setNextVertex(nextVertex.x, nextVertex.y);
    }

    public void setNextVertex (float nextVertexX, float nextVertexY) {
        this.nextVertexX = nextVertexX;
        this.nextVertexY = nextVertexY;
    }

    @Override
    public Shape.Type getType() {
        return Shape.Type.Chain;
    }

    @Override
    public Shape build() {
        ChainShape chainShape = new ChainShape();

        if(isLoop) {
            chainShape.createLoop(vertices);
        } else {
            chainShape.createChain(vertices);
        }

        if(prevVertexX != null && prevVertexY != null) {
            chainShape.setPrevVertex(prevVertexX, prevVertexY);
        }

        if(nextVertexX != null && nextVertexY != null) {
            chainShape.setNextVertex(nextVertexX, nextVertexY);
        }

        return chainShape;
    }
}
