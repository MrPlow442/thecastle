/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.box2d;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;

public class BodyTemplate implements Buildable<BodyDef> {
    public BodyDef.BodyType type = BodyDef.BodyType.StaticBody;
    public final Vector2 position = new Vector2();
    public float angle = 0;
    public final Vector2 linearVelocity = new Vector2();
    public float angularVelocity = 0;
    public float linearDamping = 0;
    public float angularDamping = 0;
    public boolean allowSleep = true;
    public boolean awake = true;
    public boolean fixedRotation = false;
    public boolean bullet = false;
    public boolean active = true;
    public float gravityScale = 1;
    public UserData userData = new UserData();

    @Override
    public BodyDef build() {
        BodyDef bodyDef = new BodyDef();

        bodyDef.type = type;
        bodyDef.position.x = position.x;
        bodyDef.position.y = position.y;
        bodyDef.angle = angle;
        bodyDef.linearVelocity.x = linearVelocity.x;
        bodyDef.linearVelocity.y = linearVelocity.y;
        bodyDef.angularVelocity = angularVelocity;
        bodyDef.linearDamping = linearDamping;
        bodyDef.angularDamping = angularDamping;
        bodyDef.allowSleep = allowSleep;
        bodyDef.awake = awake;
        bodyDef.fixedRotation = fixedRotation;
        bodyDef.bullet = bullet;
        bodyDef.active = active;
        bodyDef.gravityScale = gravityScale;
        
        return bodyDef;
    }
}
