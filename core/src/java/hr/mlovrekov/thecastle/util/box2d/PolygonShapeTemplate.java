/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.box2d;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

public class PolygonShapeTemplate extends ShapeTemplate {

    private VertexDef vertexDef;
    private BoxDef boxDef;

    public void set (Vector2[] vertices) {
        this.vertexDef = new VertexDef(new float[vertices.length * 2]);
        for (int i = 0, j = 0; i < vertices.length * 2; i += 2, j++) {
            this.vertexDef.vertices[i] = vertices[j].x;
            this.vertexDef.vertices[i + 1] = vertices[j].y;
        }
    }

    public void set (float[] vertices) {
        this.vertexDef = new VertexDef(vertices);
    }

    public void set (float[] vertices, int offset, int len) {
        this.vertexDef = new VertexDef(vertices, offset, len);
    }

    public void setAsBox (float hx, float hy) {
        this.boxDef = new BoxDef(hx, hy);
    }

    public void setAsBox (float hx, float hy, Vector2 center, float angle) {
        this.boxDef = new BoxDef(hx, hy, center, angle);
    }

    @Override
    public Shape.Type getType() {
        return Shape.Type.Polygon;
    }

    private class BoxDef {
        public final float hx;
        public final float hy;
        public final Vector2 center;
        public final Float angle;

        public BoxDef(float hx, float hy) {
            this.hx = hx;
            this.hy = hy;
            this.center = null;
            this.angle = null;
        }

        public BoxDef(float hx, float hy, Vector2 center, Float angle) {
            this.hx = hx;
            this.hy = hy;
            this.center = center;
            this.angle = angle;
        }
    }

    private class VertexDef {
        public final float[] vertices;
        public final Integer offset;
        public final Integer len;

        public VertexDef(float[] vertices) {
            this.vertices = vertices;
            this.offset = null;
            this.len = null;
        }

        public VertexDef(float[] vertices, int offset, int len) {
            this.vertices = vertices;
            this.offset = offset;
            this.len = len;
        }
    }

    @Override
    public Shape build() {
        PolygonShape polygonShape = new PolygonShape();

        if(boxDef != null) {
            if(boxDef.center != null && boxDef.angle != null) {
                polygonShape.setAsBox(boxDef.hx, boxDef.hy, boxDef.center, boxDef.angle);
            } else {
                polygonShape.setAsBox(boxDef.hx, boxDef.hy);
            }
        }

        if(vertexDef != null) {
            if(vertexDef.offset != null && vertexDef.len != null) {
                polygonShape.set(vertexDef.vertices, vertexDef.offset, vertexDef.len);
            } else {
                polygonShape.set(vertexDef.vertices);
            }
        }

        return polygonShape;
    }
}
