/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.component;

import com.artemis.Component;

public class Box2dComponent extends Component {

    public String name;

    public float b2PositionX, b2PositionY;
    public float worldCenterX, worldCenterY;
    public float localCenterX, localCenterY;

    public float angle;

    public float linearVelocityX, linearVelocityY;
    public float angularVelocity;

    public float mass;
    public float centerOfMassX, centerOfMassY;
    public float I;

    public boolean isBullet;
    public boolean isActive;

    public boolean isAwake;

    public byte footContacts;

    public boolean isGrounded() {
        return footContacts > 0;
    }

}
