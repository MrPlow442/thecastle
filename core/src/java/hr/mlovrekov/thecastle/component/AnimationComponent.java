/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.component;

import com.artemis.Component;

import java.util.HashMap;
import java.util.Map;

public class AnimationComponent extends Component {
    public String animationPackageName;
    public boolean flipX, flipY;
    public boolean center = true;
    public float stateTime;
    public float scale = 1f;
    public Float rotOriginX;
    public Float rotOriginY;
    public float r = 1f, g = 1f, b = 1f, a = 1f;
    public Layer layer = Layer.DEFAULT;

    public enum Layer {
        BACKGROUND,
        DEFAULT,
        FOREGROUND
    }
}
