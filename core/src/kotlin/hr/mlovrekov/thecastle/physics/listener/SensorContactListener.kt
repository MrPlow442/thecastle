/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.physics.listener

import com.badlogic.gdx.physics.box2d.*
import hr.mlovrekov.thecastle.util.box2d.*
import hr.mlovrekov.thecastle.util.box2d.util.listener.*
import hr.mlovrekov.thecastle.component.*

//TODO: rework this
class SensorContactListener : ContactListenerAdapter() {

    override fun beginContact(contact: Contact) {
        val fixtureA = contact.fixtureA
        val bodyA = fixtureA.body

        if (fixtureIsFoot(fixtureA)) {
            val bodyAUserData = bodyA.userData!! as UserData
            val box2dComponent = bodyAUserData["box2dComponent"]!! as Box2dComponent
            ++box2dComponent.footContacts
        }

        val fixtureB = contact.fixtureB
        val bodyB = fixtureB.body

        if (fixtureIsFoot(fixtureB)) {
            val bodyBUserData = bodyB.userData!! as UserData
            val box2dComponent = bodyBUserData["box2dComponent"]!! as Box2dComponent
            ++box2dComponent.footContacts
        }
    }

    override fun endContact(contact: Contact) {
        val fixtureA = contact.fixtureA
        val bodyA = fixtureA.body

        if (fixtureIsFoot(fixtureA)) {
            val bodyAUserData = bodyA.userData!! as UserData
            val box2dComponent = bodyAUserData["box2dComponent"]!! as Box2dComponent
            --box2dComponent.footContacts
        }

        val fixtureB = contact.fixtureB
        val bodyB = fixtureB.body

        if (fixtureIsFoot(fixtureB)) {
            val bodyBUserData = bodyB.userData!! as UserData
            val box2dComponent = bodyBUserData["box2dComponent"]!! as Box2dComponent
            --box2dComponent.footContacts
        }
    }

    private fun fixtureIsFoot(fixture: Fixture): Boolean {
        val fixtureUserData = if (fixture.userData != null) fixture.userData as UserData else return false

        return fixtureUserData.getOrDefault("name",  "N/A") == "foot"
    }

}
