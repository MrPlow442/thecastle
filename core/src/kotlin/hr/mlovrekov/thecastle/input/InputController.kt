/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.input

interface InputController {
    object Inputs {
        const val LEFT = 0
        const val RIGHT = 1
        const val UP = 2
        const val DOWN = 3
        const val JUMP = 4
        const val ACCEPT = 5
        const val DECLINE = 6

        const val TOTAL_INPUTS = 7
    }

    fun addHandler(handler: InputHandler)

    fun addHandler(index: Int, handler: InputHandler)

    fun removeHandler(handler: InputHandler)

    fun removeHandler(index: Int)

    fun process()

    fun activate(key: Int)

    fun deactivate(key: Int)

    fun isInputActive(key: Int): Boolean

    fun isInputActivated(key: Int): Boolean

    fun isInputDeactivated(key: Int): Boolean

}