/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.input

import com.badlogic.gdx.utils.Array as GdxArray

class GameInputController: InputController {

    private val inputs = BooleanArray(InputController.Inputs.TOTAL_INPUTS)
    private val previousInputs = BooleanArray(InputController.Inputs.TOTAL_INPUTS)

    private val handlerMultiplexer = InputHandlerMultiplexer(4)

    override fun addHandler(handler: InputHandler) {
        handlerMultiplexer.addHandler(handler)
    }

    override fun addHandler(index: Int, handler: InputHandler) {
        handlerMultiplexer.addHandler(index, handler)
    }

    override fun removeHandler(handler: InputHandler) {
        handlerMultiplexer.removeHandler(handler)
    }

    override fun removeHandler(index: Int) {
        handlerMultiplexer.removeHandler(index)
    }

    override fun process() {

        for (i in 0..InputController.Inputs.TOTAL_INPUTS - 1) {
            if(isInputActive(i)) {
                handlerMultiplexer.inputActive(i)
            }

            if(isInputActivated(i)) {
                handlerMultiplexer.inputActivated(i)
            }

            if(isInputDeactivated(i)) {
                handlerMultiplexer.inputDeactivated(i)
            }
            previousInputs[i] = inputs[i]
        }
    }

    override fun activate(key: Int) {
        inputs[key] = true
    }

    override fun deactivate(key: Int) {
        inputs[key] = false
    }

    override fun isInputActive(key: Int): Boolean {
        return inputs[key]
    }

    override fun isInputActivated(key: Int): Boolean {
        return !previousInputs[key] && inputs[key]
    }

    override fun isInputDeactivated(key: Int): Boolean {
        return previousInputs[key] && !inputs[key]
    }

    private class InputHandlerMultiplexer(private val capacity: Int) : InputHandler {

        private val handlers = GdxArray<InputHandler>(capacity)

        fun addHandler(handler: InputHandler) {
            handlers.add(handler)
        }

        fun addHandler(index: Int, handler: InputHandler) {
            handlers.set(index, handler)
        }

        fun removeHandler(handler: InputHandler) {
            handlers.removeValue(handler, true)
        }

        fun removeHandler(index: Int) {
            handlers.removeIndex(index)
        }

        override fun inputActive(key: Int): Boolean {
            for(handler in handlers) {
                if(handler.inputActive(key)) {
                    return true
                }
            }
            return false
        }

        override fun inputActivated(key: Int): Boolean {
            for(handler in handlers) {
                if(handler.inputActivated(key)) {
                    return true
                }
            }
            return false
        }

        override fun inputDeactivated(key: Int): Boolean {
            for(handler in handlers) {
                if(handler.inputDeactivated(key)) {
                    return true
                }
            }
            return false
        }

    }
}