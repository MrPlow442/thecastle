/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.input

import com.badlogic.gdx.*

class NullInputController: InputController {
    override fun addHandler(handler: InputHandler) { Gdx.app.log("LOCATOR - NullInputController", "Add handler called on NullInputController") }

    override fun addHandler(index: Int, handler: InputHandler) { Gdx.app.log("LOCATOR - NullInputController", "Add handler called on NullInputController") }

    override fun removeHandler(handler: InputHandler) {}

    override fun removeHandler(index: Int) {}

    override fun process() {}

    override fun activate(key: Int) {}

    override fun deactivate(key: Int) {}

    override fun isInputActive(key: Int): Boolean = false

    override fun isInputActivated(key: Int): Boolean = false

    override fun isInputDeactivated(key: Int): Boolean = false

}