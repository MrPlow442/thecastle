/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.state

import com.badlogic.gdx.*
import com.badlogic.gdx.utils.*
import java.util.*

class StateManager(initialStateCount: Int) : Disposable {

    private val states = ArrayDeque<State>(initialStateCount)

    fun push(state: State) {
        states.peekFirst()?.let {
            it.pause()
            it.hide()
        }

        state.stateManager = this
        state.create()
        state.show()
        state.resize(Gdx.graphics.width, Gdx.graphics.height)
        states.add(state)
    }

    fun pop() {
        states.pollFirst()?.let {
            it.hide()
            it.dispose()
        }

        states.peekFirst()?.let {
            it.show()
            it.resume()
        }
    }

    fun swapWith(state: State) {
        pop()
        push(state)
    }

    fun pause() {
        states.peekFirst()?.pause()
    }

    fun resize(width: Int, height: Int) {
        for (state in states) {
            state.resize(width, height)
        }
    }

    fun render() {
        val it = states.descendingIterator()
        while (it.hasNext()) {
            it.next().render(Gdx.graphics.deltaTime)
        }
    }

    fun resume() {
        states.peekFirst()?.resume()
    }

    override fun dispose() {
        for (state in states) {
            state.hide()
            state.dispose()
        }
        states.clear()
    }
}