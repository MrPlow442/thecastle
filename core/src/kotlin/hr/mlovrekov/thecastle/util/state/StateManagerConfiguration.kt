/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.state

import java.util.*

class StateManagerConfiguration {

    var initialStateCount = 2
    val injectables: MutableMap<Class<*>, Any> = HashMap()

    fun setInitialStateCount(initialStateCount: Int): StateManagerConfiguration {
        this.initialStateCount = initialStateCount
        return this
    }

    fun register(obj: Any): StateManagerConfiguration {
        return register(obj.javaClass, obj)
    }

    fun register(clazz: Class<*>, obj: Any): StateManagerConfiguration {
        injectables.put(clazz, obj)
        return this
    }
}