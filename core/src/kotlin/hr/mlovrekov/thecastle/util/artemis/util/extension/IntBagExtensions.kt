/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.artemis.util.extension

import com.artemis.utils.*

operator fun IntBag.get(index: Int) = get(index)
operator fun IntBag.set(index: Int, value: Int) = set(index, value)

inline fun IntBag.forEach(action: (Int) -> Unit) {
    for (i in 0..size() - 1) {
        action(get(i))
    }
}

inline fun IntBag.forEachIndexed(action: (Int, Int) -> Unit) {
    for (i in 0..size() - 1) {
        action(i, get(i))
    }
}

operator fun IntBag.iterator(): Iterator<Int> = object : Iterator<Int> {
    private var index = 0
    override fun hasNext() = index < size()
    override fun next() = get(index++)
}