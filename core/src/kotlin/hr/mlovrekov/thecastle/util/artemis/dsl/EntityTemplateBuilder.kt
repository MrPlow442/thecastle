/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.artemis.dsl

import com.artemis.*
import hr.mlovrekov.thecastle.util.artemis.*
import hr.mlovrekov.thecastle.util.gdx.*

inline fun entity(entityTemplateBuilder: EntityTemplate.() -> Unit): EntityTemplate {
    val entityTemplate = EntityTemplate()
    entityTemplate.entityTemplateBuilder()
    return entityTemplate
}

inline fun <T : Component> EntityTemplate.component(clazz: Class<T>, componentBuilder: T.() -> Unit) {
    val component = clazz.newInstance()
    component.componentBuilder()
    components += component
}

/**
 * Since Kotlin doesn't yet support initializers in inline functions this serves as a workaround
 * FIXME: replace this function by initializer when inline defaults become available in Kotlin
 */
fun <T : Component> EntityTemplate.component(clazz: Class<T>) {
    val component = clazz.newInstance()
    components += component
}