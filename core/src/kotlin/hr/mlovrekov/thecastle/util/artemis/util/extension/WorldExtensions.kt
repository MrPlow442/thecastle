/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.artemis.util.extension

import com.artemis.*
import com.rits.cloning.*
import hr.mlovrekov.thecastle.util.artemis.*

fun World.createEntityFromEntityTemplate(entityTemplate: EntityTemplate): Entity {
    val entity = this.createEntity()
    val entityEdit = entity.edit()
    val cloner = Cloner()
//    cloner.isDumpClonedClasses = true
    for (component in entityTemplate.components) {
        entityEdit.add(cloner.deepClone(component))
    }
    return entity
}