/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.animation

import com.badlogic.gdx.graphics.g2d.*
import hr.mlovrekov.thecastle.component.*
import java.util.*

class AnimationPackage(private val actionAnimations: HashMap<ActionComponent.Action, Animation>) {

    init {
        if (!actionAnimations.containsKey(ActionComponent.Action.DEFAULT)) {
            throw RuntimeException("Animation package must contain a DEFAULT animation")
        }
    }

    constructor(actionAnimations: HashMap<ActionComponent.Action, Animation>, defaultAnimation: Animation) : this(
            HashMap<ActionComponent.Action, Animation>(actionAnimations.size + 1).apply {
                putAll(actionAnimations)
                put(ActionComponent.Action.DEFAULT, defaultAnimation)
            })

    constructor(vararg pairs: Pair<ActionComponent.Action, Animation>, defaultAnimation: Animation) : this(HashMap<ActionComponent.Action, Animation>(
            pairs.size + 1).apply { putAll(pairs) }, defaultAnimation)

    constructor(defaultAnimation: Animation) : this(hashMapOf(ActionComponent.Action.DEFAULT to defaultAnimation))

    val all: Map<ActionComponent.Action, Animation> get() = actionAnimations

    val actions: Set<ActionComponent.Action> get() = actionAnimations.keys

    val default: Animation get() = actionAnimations[ActionComponent.Action.DEFAULT]!!

    operator fun get(action: ActionComponent.Action): Animation? = actionAnimations[action]

    operator fun contains(action: ActionComponent.Action) = actionAnimations.containsKey(action)
}