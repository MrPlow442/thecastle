/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.render

import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.math.*


interface Clearable {

    fun setClearColor(color: Color) {
        Gdx.gl.glClearColor(color.r, color.g, color.b, color.a)
    }

    fun setClearColor(red: Float, green: Float, blue: Float, alpha: Float) {
        Gdx.gl.glClearColor(MathUtils.clamp(red, 0f, 1f),
                            MathUtils.clamp(green, 0f, 1f),
                            MathUtils.clamp(blue, 0f, 1f),
                            MathUtils.clamp(alpha, 0f, 1f))
    }

    fun setClearColor(red: Int, green: Int, blue: Int, alpha: Int) {
        Gdx.gl.glClearColor(MathUtils.clamp(red / 255f, 0f, 1f),
                            MathUtils.clamp(green / 255f, 0f, 1f),
                            MathUtils.clamp(blue / 255f, 0f, 1f),
                            MathUtils.clamp(alpha / 255f, 0f, 1f))
    }

    fun clear(): Unit {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
    }
}