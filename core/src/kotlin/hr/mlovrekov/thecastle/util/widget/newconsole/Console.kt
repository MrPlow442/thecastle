/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.widget.newconsole

class Console {

    companion object {
        private const val MAX_ROWS = 25
        private const val MAX_COLUMNS = 1024
        private const val COMMAND_PREFIX = '>'
    }

    private val buffer = ConsoleBuffer(MAX_ROWS, MAX_COLUMNS)


    fun printLine(message: CharArray) {
        buffer.write(message)
    }

    fun printLine(message: String) {
        buffer.write(message)
    }

    fun execute(command: String) {
        printLine("$COMMAND_PREFIX $command")
        val parsedArguments = ConsoleParser(command.substring(0, Math.min(command.length, MAX_COLUMNS))).parse()
//        printLine("Executing ${parseResult.command} with args: ${parseResult.arguments}")
    }

    override fun toString(): String {
        val rows = buffer.rows

        val builder = StringBuilder(rows.size * MAX_COLUMNS)

        rows.forEach {
            builder.append(it).append('\n')
        }

        return builder.toString()
    }
}