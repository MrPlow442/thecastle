/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.widget.console

import com.badlogic.gdx.utils.*
import hr.mlovrekov.thecastle.util.gdx.*

class ConsoleParser {

    private val commands = ObjectMap<String, ConsoleCommand>(15)

    companion object {
        @JvmStatic private val COMMAND_REGEX = Regex("([\\w\\-\\.]+|\\[(?:(?:[\\w\\-\\s\\.]+:[\\w\\-\\s\\.]+),?)+]|\\[(?:(?:[\\w\\-\\s\\.])+?,?)+])")
    }

    init {
        registerCommand("list", ListConsoleCommand())
        registerCommand("help", HelpConsoleCommand())
    }

    fun parse(line: String): String {

        val arguments = COMMAND_REGEX.findAll(line).map {
            val value = it.groupValues[1]
            if (value.startsWith("[") && value.endsWith("]")) {
                val collection = value.substring(1, value.length - 1)
                if(isMap(collection)) {
                    parseMap(collection)
                } else if(isArray(collection)) {
                    parseArray(collection)
                } else {
                    null
                }
            } else {
                value
            }
        }.filterNotNull().toMutableList()

        val commandName = arguments.removeAt(0).toString()

        val args = arguments.toTypedArray()

        if(commands.containsKey(commandName)) {
            return commands[commandName].execute(args)
        } else {
            return "UNKNOWN COMMAND '$commandName'"
        }
    }

    fun registerCommand(key: String, command: ConsoleCommand) {
        commands[key] = command
    }

    fun unregisterCommand(key: String) {
        commands.remove(key)
    }

    private fun isMap(text: String) = text.split(',').all { it.contains(':') }
    private fun isArray(text: String) = text.isBlank() || text.contains(',')

    private fun parseArray(text: String): Array<String> {
        return text.split(',')
                .map { it.trim() }
                .toTypedArray()
    }

    private fun parseMap(text: String): Map<String, String> {
        return text.split(',')
                .associate {
                    val values = it.split(':')
                    Pair(values[0], values[1])
                }
    }

    private inner class ListConsoleCommand : ConsoleCommand {
        override val description: String = "Displays a list of available commands"
        override val usage: String = """Usage: list [options]
        |Arguments:
        |-i --inline - shows available commands inline without descriptions
        """.trimMargin()

        override fun execute(args: Array<Any>): String {

            val inline = args.contains("-i") || args.contains("--inline")

            if(inline) {
                return "Available commands: ${commands.keys().joinToString(" ")}"
            }

            return "Available commands: ${commands.map { "${it.key} - ${it.value.description}" }}"
        }
    }

    private inner class HelpConsoleCommand: ConsoleCommand {
        override val description: String = "Shows help information"
        override val usage: String = """Usage: help (optional)[command]
        |Arguments:
        |if invoked without arguments this page is displayed.
        |[command] - shows the description and instructions on how to use the specified command.
        """

        override fun execute(args: Array<Any>): String {
            if(args.isEmpty() || args[0] !in commands.keys()) {
                return "Help\n$description\n$usage"
            } else {
                val commandName = args[0] as String
                val command = commands[commandName]

                return "$commandName\n${command.description}\n${command.usage}"
            }
        }
    }
}