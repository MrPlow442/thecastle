/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.widget.console

import com.badlogic.gdx.*
import com.badlogic.gdx.scenes.scene2d.*
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.*
import java.util.*

class Console(skin: Skin) : Window("Console", skin) {

    /* PARSER */
    val parser = ConsoleParser()

    /* UI */
    companion object {
        private const val ROWS = 25
        private const val HISTORY_ROWS = 100
        private const val COLUMNS = 1024

        private const val NEW_LINE = '\n'
        private const val LINE_FEED = '\r'
    }

    private val consoleLogBuffer = ArrayDeque<String>(ROWS)
    private val history = ArrayList<String>(HISTORY_ROWS)
    private var historyIndex = 0

    // SCENE2D ELEMENTS
    private val consoleLogLabel = Label("", skin)
    private val consoleLogScrollPane = ScrollPane(consoleLogLabel, skin)
    private val consoleTextField = TextField("", skin)

    init {
        padTop(20f)
        width = 500f
        height = 250f
        isMovable = true
        isModal = true
        isResizable = true
        setKeepWithinStage(true)

        consoleLogLabel.apply {
            setWrap(true)
        }

        consoleLogScrollPane.apply {
            setFadeScrollBars(false)
            setScrollbarsOnTop(false)
            setOverscroll(false, false)
        }

        consoleTextField.apply {
            setFocusTraversal(false)
            maxLength = COLUMNS
            setTextFieldListener { textField, key ->
                if (key == LINE_FEED || key == NEW_LINE) {
                    val command = textField.text
                    textField.text = ""

                    addRow(command)
                    addHistory(command)
                    addRow(parser.parse(command))
                }
            }
            addListener(object : InputListener() {
                override fun keyDown(event: InputEvent, keycode: Int): Boolean {
                    return when (keycode) {
                        Input.Keys.UP   -> {
                            text = getPreviousHistory()
                            true
                        }
                        Input.Keys.DOWN -> {
                            text = getNextHistory()
                            true
                        }
                        else            -> false
                    }
                }
            })
        }

        defaults()
        add(consoleLogScrollPane).expand().fill()
        row()
        add(consoleTextField).expandX().fillX()
        updateConsoleLogLabel()
    }

    fun registerCommand(key: String, command: ConsoleCommand) {
        parser.registerCommand(key, command)
    }

    fun unregisterCommand(key: String) {
        parser.unregisterCommand(key)
    }

    private fun updateConsoleLogLabel() {
        consoleLogLabel.setText(StringBuilder(consoleLogBuffer.size * COLUMNS).apply {
            consoleLogBuffer.forEach { append(it).append("\n") }
        }.toString())
        consoleLogScrollPane.scrollPercentY = 100f
    }

    private fun clearConsoleLog() {
        consoleLogBuffer.clear()
        updateConsoleLogLabel()
    }

    private fun addRow(value: String) {
        if (consoleLogBuffer.size >= ROWS) {
            consoleLogBuffer.pop()
        }

        consoleLogBuffer.add(if (value.length >= COLUMNS) value.substring(0, COLUMNS) else value)
        updateConsoleLogLabel()
    }

    private fun addHistory(value: String) {
        if (value.isBlank()) {
            return
        }

        if (history.size >= HISTORY_ROWS) {
            history.removeAt(0)
        }

        history.add(if (value.length >= COLUMNS) value.substring(0, COLUMNS) else value)
        historyIndex = history.size - 1
    }

    private fun getHistory(index: Int) = if (index in history.indices) history[index] else ""

    fun getAndIncrementHistoryIndex() = if (historyIndex < history.size) historyIndex++ else historyIndex

    fun getAndDecrementHistoryIndex() = if (historyIndex > 0) historyIndex-- else historyIndex

    private fun getPreviousHistory() = getHistory(getAndDecrementHistoryIndex())

    private fun getNextHistory() = getHistory(getAndIncrementHistoryIndex())
}