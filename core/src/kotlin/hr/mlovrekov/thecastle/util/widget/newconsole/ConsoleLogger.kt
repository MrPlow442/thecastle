/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.widget.newconsole

import hr.mlovrekov.thecastle.util.logging.*

class ConsoleLogger(val console: Console) : ApplicationLogger {

    companion object {
        private const val LOG_COLOR = "WHITE"
        private const val DEBUG_COLOR = "GOLDENROD"
        private const val ERROR_COLOR = "RED"
    }

    override fun log(tag: String, message: String) {
        console.printLine("[$LOG_COLOR]$tag:$message")
    }

    override fun log(tag: String, message: String, exception: Throwable) {
        console.printLine("[$LOG_COLOR]$tag:$message ${exception.message}")
    }

    override fun error(tag: String, message: String) {
        console.printLine("[$ERROR_COLOR]$tag:$message")
    }

    override fun error(tag: String, message: String, exception: Throwable) {
        console.printLine("[$ERROR_COLOR]$tag:$message ${exception.message}")
    }

    override fun debug(tag: String, message: String) {
        console.printLine("[$DEBUG_COLOR]$tag:$message")
    }

    override fun debug(tag: String, message: String, exception: Throwable) {
        console.printLine("[$DEBUG_COLOR]$tag:$message ${exception.message}")
    }

}