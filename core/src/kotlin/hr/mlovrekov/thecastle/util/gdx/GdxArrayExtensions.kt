/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.gdx

import com.badlogic.gdx.utils.*
import java.util.*
import kotlin.comparisons.*
import com.badlogic.gdx.utils.Array as GdxArray

inline fun <reified T> gdxArrayOf(vararg elements: T): GdxArray<T> = GdxArray(elements)

inline fun <reified T> GdxArray(capacity: Int, init: (Int) -> T): GdxArray<T> {
    val result = GdxArray<T>(capacity)
    for (i in 0..capacity - 1) {
        result += init(i)
    }
    return result
}

/**
 * Sorts elements in the list in-place according to natural sort order of the value returned by specified [selector] function.
 */
inline fun <T, R : Comparable<R>> GdxArray<T>.sortBy(crossinline selector: (T) -> R?): Unit {
    if (size > 1) sortWith(compareBy(selector))
}

/**
 *  Sorts elements in the list in-place according to order specified with [comparator].
 */
fun <T> GdxArray<T>.sortWith(comparator: Comparator<in T>): Unit {
    if (size > 1) Sort.instance().sort(this, comparator)
}

/**
 * Creates a new LibGdx array containing contents of both lhs and rhs LibGdx arrays
 * @return a new LibGdx array
 */
operator fun <T> GdxArray<T>.plus(other: GdxArray<T>): GdxArray<T> {
    val result = GdxArray<T>(size + other.size)
    result.addAll(this)
    result.addAll(other)
    return result
}

/**
 * Creates a new LibGdx array containing contents of lhs LibGdx array and rhs element
 * @return a new LibGdx array
 */
operator fun <T> GdxArray<T>.plus(element: T): GdxArray<T> {
    val result = GdxArray<T>(size + 1)
    result.addAll(this)
    result.add(element)
    return result
}

/**
 * Creates a new LibGdx array containing contents of lhs array with contents of rhs LibGdx array removed
 * @return a new LibGdx array
 */
operator fun <T> GdxArray<T>.minus(other: GdxArray<T>): GdxArray<T> {
    val result = GdxArray<T>(size)
    result.addAll(this)
    result.removeAll(other, true)
    return result
}

/**
 * Creates a new LibGdx array containing contents of lhs array with rhs element removed
 * @return a new LibGdx array
 */
operator fun <T> GdxArray<T>.minus(element: T): GdxArray<T> {
    val result = GdxArray<T>(size)
    result.addAll(this)
    result.removeValue(element, true)
    return result
}

/**
 * Removes all elements of rhs LibGdx array from this LibGdx array
 */
operator fun <T> GdxArray<T>.minusAssign(other: GdxArray<T>) {
    removeAll(other, true)
}

/**
 * Removes rhs element from this LibGdx array
 */
operator fun <T> GdxArray<T>.minusAssign(element: T) {
    removeValue(element, true)
}

/**
 * Adds all elements from rhs LibGdx array onto this array
 */
operator fun <T> GdxArray<T>.plusAssign(other: GdxArray<T>) = addAll(other)

/**
 * Adds rhs element into this LibGdx array
 */
operator fun <T> GdxArray<T>.plusAssign(element: T) = add(element)

operator fun <T> GdxArray<T>.get(index: Int): T = get(index)

operator fun <T> GdxArray<T>.set(index: Int, value: T): T {
    set(index, value)
    return value
}

