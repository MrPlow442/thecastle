/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.gdx

import com.badlogic.gdx.utils.*


operator fun <K, V> ObjectMap<K, V>.get(key: K): V? = get(key)
operator fun <K, V> ObjectMap<K, V>.set(key: K, value: V) = put(key, value)

operator fun <K, V> ObjectMap.Entry<K, V>.component1() = key
operator fun <K, V> ObjectMap.Entry<K, V>.component2() = value

operator fun <K, V> ObjectMap<K, V>.contains(key: K) = containsKey(key)

fun <K, V> mutableObjectMapOf(vararg pairs: Pair<K, V>) = ObjectMap<K, V>(pairs.size).apply { pairs.forEach { put(it.first, it.second) } }

