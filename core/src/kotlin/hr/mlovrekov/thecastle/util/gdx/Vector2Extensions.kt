/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.gdx

import com.badlogic.gdx.math.*

/**
 * Creates a new Vector2 which is a sum of lhs and rhs vectors
 * @return new vector
 */
operator fun Vector2.plus(other: Vector2) = Vector2(this).add(other)

/**
 * Creates a new Vector2 which is a difference of lhs and rhs vectors
 * @return new vector
 */
operator fun Vector2.minus(other: Vector2) = Vector2(this).sub(other)

/**
 * Creates a new Vector2 which is a result of multiplication of lhs and rhs vectors
 * @return new vector
 */
operator fun Vector2.times(other: Vector2) = Vector2(this).scl(other)

/**
 * Creates a new Vector2 which is scaled by scalar
 * @return new vector
 */
operator fun Vector2.times(scalar: Float) = Vector2(this).scl(scalar)

/**
 * Creates a new Vector2 which is a result of division between lhs and rhs vectors
 * @return new vector
 */
operator fun Vector2.div(other: Vector2): Vector2 {
    val result = Vector2(this)
    result.x /= other.x
    result.y /= other.y
    return result
}

/**
 * Adds rhs vector to this vector
 */
operator fun Vector2.plusAssign(other: Vector2) {
    add(other)
}

/**
 * Subtracts rhs vector from this vector
 */
operator fun Vector2.minusAssign(other: Vector2) {
    sub(other)
}

/**
 * Multiplies this vector with rhs vector
 */
operator fun Vector2.timesAssign(other: Vector2) {
    scl(other)
}

/**
 * Scales this vector with rhs scalar
 */
operator fun Vector2.timesAssign(scalar: Float) {
    scl(scalar)
}

/**
 * Divides this vector with rhs vector
 */
operator fun Vector2.divAssign(other: Vector2) {
    x /= other.x
    y /= other.y
}

operator fun Vector2.component1() = this.x
operator fun Vector2.component2() = this.y


