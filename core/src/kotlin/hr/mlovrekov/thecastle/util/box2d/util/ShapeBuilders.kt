/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.box2d.util

import com.badlogic.gdx.maps.objects.*
import com.badlogic.gdx.math.*
import com.badlogic.gdx.physics.box2d.*
import hr.mlovrekov.thecastle.config.*


object ShapeBuilders {

    fun fromRectangleMapObject(mapObject: RectangleMapObject): PolygonShape {
        val rect = mapObject.rectangle
        val b2dWidth = rect.width * INVERSE_SCALE
        val b2dHeight = rect.height * INVERSE_SCALE
        val polygon = PolygonShape()

        val vertices: Array<Vector2> = arrayOf(
                Vector2(0f, 0f),
                Vector2(0f, b2dHeight),
                Vector2(b2dWidth, b2dHeight),
                Vector2(b2dWidth, 0f),
                Vector2(0f, 0f))

        polygon.set(vertices)
        return polygon
    }

}