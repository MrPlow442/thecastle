/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.box2d.util.dsl

import com.badlogic.gdx.math.*
import hr.mlovrekov.thecastle.util.box2d.*
import hr.mlovrekov.thecastle.util.box2d.*
import hr.mlovrekov.thecastle.util.gdx.*

inline fun box2dEntityTemplate(entityBuilder: Box2dEntityTemplate.() -> Unit): Box2dEntityTemplate {
    val entityTemplate = Box2dEntityTemplate()
    entityTemplate.entityBuilder()
    return entityTemplate
}

inline fun Box2dEntityTemplate.bodyTemplate(bodyBuilder: BodyTemplate.() -> Unit) {
    body.bodyBuilder()
}

inline fun Box2dEntityTemplate.fixtureTemplate(fixtureBuilder: FixtureTemplate.() -> Unit) {
    val fixtureTemplate = FixtureTemplate()
    fixtureTemplate.fixtureBuilder()
    fixtures += fixtureTemplate
}

inline fun BodyTemplate.userData(userDataBuilder: UserData.() -> Unit) {
    userData.userDataBuilder()
}

inline fun FixtureTemplate.userData(userDataBuilder: UserData.() -> Unit) {
    userData.userDataBuilder()
}

inline fun FixtureTemplate.polygonShape(polygonShapeBuilder: PolygonShapeTemplate.() -> Unit) {
    shape = PolygonShapeTemplate()
    (shape as PolygonShapeTemplate).polygonShapeBuilder()
}

inline fun FixtureTemplate.circleShape(circleShapeBuilder: CircleShapeTemplate.() -> Unit) {
    shape = CircleShapeTemplate()
    (shape as CircleShapeTemplate).circleShapeBuilder()
}

inline fun FixtureTemplate.edgeShape(edgeShapeBuilder: EdgeShapeTemplate.() -> Unit) {
    shape = EdgeShapeTemplate()
    (shape as EdgeShapeTemplate).edgeShapeBuilder()
}

inline fun FixtureTemplate.chainShape(chainShapeBuilder: ChainShapeTemplate.() -> Unit) {
    shape = ChainShapeTemplate()
    (shape as ChainShapeTemplate).chainShapeBuilder()
}

fun PolygonShapeTemplate.center(x: Float, y: Float): Vector2 {
    return Vector2(x, y)
}

fun CircleShapeTemplate.position(x: Float, y: Float) {
    position = Vector2(x, y)
}