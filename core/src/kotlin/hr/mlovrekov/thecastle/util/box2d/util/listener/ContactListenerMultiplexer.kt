/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.util.box2d.util.listener

import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.utils.*
import com.badlogic.gdx.utils.Array as GdxArray

private const val DEFAULT_CAPACITY = 5

class ContactListenerMultiplexer(val contactListeners: com.badlogic.gdx.utils.Array<ContactListener> = com.badlogic.gdx.utils.Array<ContactListener>(
        DEFAULT_CAPACITY))
: ContactListener, Disposable {

    constructor(capacity: Int) : this(com.badlogic.gdx.utils.Array<ContactListener>(capacity))

    constructor(vararg contactListeners: ContactListener) : this(com.badlogic.gdx.utils.Array<ContactListener>(
            contactListeners))

    // LISTENER METHODS
    override fun beginContact(contact: Contact) {
        contactListeners.forEach { it.beginContact(contact) }
    }

    override fun endContact(contact: Contact) {
        contactListeners.forEach { it.endContact(contact) }
    }

    override fun preSolve(contact: Contact, oldManifold: Manifold) {
        contactListeners.forEach { it.preSolve(contact, oldManifold) }
    }

    override fun postSolve(contact: Contact, impulse: ContactImpulse) {
        contactListeners.forEach { it.postSolve(contact, impulse) }
    }

    // COLLECTION METHODS
    fun indexOf(contactListener: ContactListener): Int {
        return contactListeners.indexOf(contactListener, true)
    }

    fun add(contactListener: ContactListener) {
        contactListeners.add(contactListener)
    }

    fun remove(contactListener: ContactListener, identity: Boolean = true): Boolean {
        return contactListeners.removeValue(contactListener, identity)
    }

    fun clear() {
        contactListeners.clear()
    }

    fun size(): Int {
        return contactListeners.size
    }

    // OPERATORS
    operator fun get(index: Int): ContactListener {
        return contactListeners.get(index)!!
    }

    operator fun plusAssign(contactListener: ContactListener) {
        add(contactListener)
    }

    operator fun minusAssign(contactListener: ContactListener) {
        remove(contactListener)
    }

    operator fun contains(contactListener: ContactListener): Boolean {
        return contactListeners.contains(contactListener, true)
    }

    // DISPOSABLE
    override fun dispose() {
        clear()
    }
}