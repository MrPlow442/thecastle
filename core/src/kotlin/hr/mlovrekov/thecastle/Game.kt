/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle

import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.*
import hr.mlovrekov.thecastle.config.*
import hr.mlovrekov.thecastle.input.*
import hr.mlovrekov.thecastle.state.*
import hr.mlovrekov.thecastle.util.locator.*
import hr.mlovrekov.thecastle.util.render.*
import hr.mlovrekov.thecastle.util.state.*

class Game(val platformProperties: PlatformProperties) : Clearable, ApplicationListener {

    private lateinit var stateManager: StateManager
    private val inputController = GameInputController()

    override fun create() {

        platformProperties.inputProcessor.controllerDelegate = inputController
        Gdx.input.inputProcessor = platformProperties.inputProcessor
        ServiceLocator.provideInputController(inputController)

        Gdx.app.getPreferences(PREFS_NAME)
                .putBoolean("debug", true)
                .flush()

        stateManager = StateManager(2)
//        stateManager.push(TestState())
        stateManager.push(UiTestState())
    }

    override fun render() {
        setClearColor(Color.BLACK)
        clear()
        inputController.process()
        stateManager.render()
    }

    override fun pause() {
        stateManager.pause()
    }

    override fun resize(width: Int, height: Int) {
        stateManager.resize(width, height)
    }

    override fun resume() {
        stateManager.resume()
    }

    override fun dispose() {
        stateManager.dispose()
    }
}
