/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.system

import com.artemis.*
import com.artemis.annotations.*
import com.badlogic.gdx.maps.tiled.*
import com.badlogic.gdx.physics.box2d.*
import hr.mlovrekov.thecastle.resource.*
import hr.mlovrekov.thecastle.util.gdx.*
import com.badlogic.gdx.utils.Array as GdxArray

class MapSystem : BaseSystem() {

    private lateinit var box2dSystem: Box2dSystem

    @Wire
    private lateinit var resources: GameStateResources

    lateinit var map: TiledMap
        private set

    private val bodies = GdxArray<Body>(15)

    //TODO: implement loading/disposing N rooms in order provided by map generator
    override fun initialize() {
        map = resources["rooms/testroom.tmx"]
        map.layers.get("Collision").objects.forEach {
            bodies += box2dSystem.createBody(it)
        }
    }

    override fun processSystem() {
        //Nothing for now
    }

    override fun dispose() {
        bodies.forEach {
            box2dSystem.destroyBody(it)
        }
        bodies.clear()
    }
}