/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.system

import com.artemis.*
import com.artemis.annotations.*
import com.badlogic.gdx.*
import hr.mlovrekov.thecastle.util.artemis.util.*
import hr.mlovrekov.thecastle.util.artemis.util.extension.*
import hr.mlovrekov.thecastle.component.*
import hr.mlovrekov.thecastle.resource.*
import hr.mlovrekov.thecastle.util.animation.*
import hr.mlovrekov.thecastle.util.gdx.*
import com.badlogic.gdx.utils.Array as GdxArray

/**
 * System which renders animations.
 */
class AnimationRenderSystem : BaseEntitySystem(Aspect.all(AnimationComponent::class.java,
                                                          PositionComponent::class.java)) {

    private lateinit var animationComponentMapper: ComponentMapper<AnimationComponent>
    private lateinit var positionComponentMapper: ComponentMapper<PositionComponent>
    private lateinit var actionComponentMapper: ComponentMapper<ActionComponent>

    private lateinit var spriteBatchSystem: SpriteBatchSystem
    private lateinit var cameraSystem: CameraSystem

    @Wire
    private lateinit var resources: GameStateResources

    /** Collection holding renderable entities, sorted by layer */
    private val sortedEntities: GdxArray<Int> = GdxArray()

    /** Batch to render to */
    private val batch by lazy { spriteBatchSystem.mainBatch }

    override fun begin() {
        batch.projectionMatrix = cameraSystem.mainCamera.combined
        batch.begin()
    }

    override fun processSystem() {
        sortedEntities.forEach {
            process(it)
        }
    }

    override fun end() {
        batch.end()
    }

    override fun inserted(entityId: Int) {
        sortedEntities.add(entityId)
        sortedEntities.sortBy { animationComponentMapper.get(it).layer }
    }

    override fun removed(entityId: Int) {
        sortedEntities.removeValue(entityId, true)
    }

    /**
     * Render animation
     *
     * @param entityId of entity whose animation will be drawn
     * @throws NullPointerException if there is an animation with a provided name doesn't exist
     */
    private fun process(entityId: Int) {
        val positionComponent = positionComponentMapper[entityId]
        val animationComponent = animationComponentMapper[entityId]

        val currentAction = if (entityId in actionComponentMapper) {
            actionComponentMapper[entityId].currentAction
        } else {
            ActionComponent.Action.DEFAULT
        }

        animationComponent.stateTime += Gdx.graphics.deltaTime
        when (currentAction) {
            ActionComponent.Action.MOVING_LEFT -> animationComponent.flipX = true
            else                               -> animationComponent.flipX = false
        }

        val animationPackage: AnimationPackage = resources[animationComponent.animationPackageName]
        val animation = animationPackage[currentAction] ?: animationPackage.default

        val frame = animation.getKeyFrame(animationComponent.stateTime, true)

        val rotOriginX: Float = if (animationComponent.rotOriginX == null) {
            frame.regionWidth * animationComponent.scale * 0.5f
        } else {
            animationComponent.rotOriginX
        }
        val rotOriginY: Float = if (animationComponent.rotOriginY == null) {
            frame.regionHeight * animationComponent.scale * 0.5f
        } else {
            animationComponent.rotOriginY
        }

        var xPos = positionComponent.x
        var yPos = positionComponent.y
        val angle = positionComponent.angle

        xPos = if (animationComponent.center) {
            xPos - frame.regionWidth * 0.5f
        } else {
            xPos
        }

        yPos = if (animationComponent.center) {
            yPos - frame.regionHeight * 0.5f
        } else {
            yPos
        }

        batch.draw(frame.texture,
                   xPos,
                   yPos,
                   rotOriginX,
                   rotOriginY,
                   frame.regionWidth * animationComponent.scale,
                   frame.regionHeight * animationComponent.scale,
                   1f,
                   1f,
                   angle,
                   frame.regionX,
                   frame.regionY,
                   frame.regionWidth,
                   frame.regionHeight,
                   animationComponent.flipX,
                   animationComponent.flipY)

        if (animation.isAnimationFinished(animationComponent.stateTime)) {
            animationComponent.stateTime = 0f
        }
    }
}
