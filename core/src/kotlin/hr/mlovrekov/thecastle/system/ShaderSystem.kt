/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.system

import com.artemis.*
import com.artemis.annotations.*
import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.glutils.*
import hr.mlovrekov.thecastle.resource.*
import hr.mlovrekov.thecastle.util.artemis.util.system.*
import hr.mlovrekov.thecastle.util.debug.*
import hr.mlovrekov.thecastle.util.render.*

class ShaderSystem : BaseSystem(), Clearable, ResizeAware {

    private lateinit var spriteBatchSystem: SpriteBatchSystem
    private lateinit var cameraSystem: CameraSystem

    @Wire
    private lateinit var resources: GameStateResources

    private val batch by lazy { spriteBatchSystem.mainBatch }

    private val scanlineShader by lazy { resources.get<ShaderProgram>("shader/scanline") }

    private lateinit var primaryFbo: FrameBuffer
    private lateinit var secondaryFbo: FrameBuffer

    override fun initialize() {
        initFbos()
        scanlineShader.validate()
    }

    override fun begin() {
        batch.shader = scanlineShader
    }

    override fun processSystem() {

    }

    override fun end() {

    }

    override fun dispose() {

    }

    override fun onResize(width: Int, height: Int) {
        initFbos()
    }

    fun ShaderProgram.validate() {
        if (!isCompiled || log.length != 0) {
            println("Shader Log: $log")
            OnScreenLogger["Shader log"] = log
        }
    }

    fun initFbos(format: Pixmap.Format = Pixmap.Format.RGBA8888, width: Int = Gdx.graphics.width, height: Int = Gdx.graphics.height, depth: Boolean = false) {
        primaryFbo = FrameBuffer(format, width, height, depth)
        secondaryFbo = FrameBuffer(format, width, height, depth)
    }
}
