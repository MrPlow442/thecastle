/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.system

import com.artemis.*
import com.artemis.systems.*
import com.badlogic.gdx.*
import com.badlogic.gdx.math.*
import hr.mlovrekov.thecastle.util.artemis.util.*
import hr.mlovrekov.thecastle.util.artemis.util.system.*
import hr.mlovrekov.thecastle.component.*

/**
 * Creates an invisible bounding box within which player can move without the camera following him.
 * As soon as the player leaves the bounds the camera will start following the player
 */
@LogicSystem
class PlayerCameraFollowSystem : IteratingSystem(
        Aspect.all(PlayerComponent::class.java,
                   PositionComponent::class.java)), ResizeAware {

    companion object {
        private const val BOUNDING_BOX_SCALE = 0.35f // 35% of screen
        private const val FOLLOW_SPEED = 5.0f
    }

    private lateinit var positionComponentMapper: ComponentMapper<PositionComponent>

    private lateinit var cameraSystem: CameraSystem

    var x: Float = 0f
        private set
    var y: Float = 0f
        private set
    var height: Float = 0f
        private set
    var width: Float = 0f
        private set
    val centerX: Float
        get() = x + width / 2

    val centerY: Float
        get() = y + height / 2

    val leftEdge: Float
        get() = x

    val rightEdge: Float
        get() = x + width

    val bottomEdge: Float
        get() = y

    val topEdge: Float
        get() = y + height


    override fun initialize() {
        resizeBounds(Gdx.graphics.width, Gdx.graphics.height)
    }

    override fun process(entityId: Int) {
        val positionComponent = positionComponentMapper[entityId]

        updateBounds(positionComponent.x, positionComponent.y)
        cameraSystem.setMainCameraPosition(x = centerX,
                                           y = centerY,
                                           xOffset = 0f,
                                           yOffset = 10f)
        cameraSystem.setBox2dDebugCameraPosition(x = centerX,
                                                 y = centerY,
                                                 xOffset = 0f,
                                                 yOffset = 10f)
    }

    override fun onResize(width: Int, height: Int) {
        resizeBounds(width, height)
    }

    private fun resizeBounds(width: Int, height: Int) {
        val boundWidth = width * CameraSystem.ZOOM
        val boundHeight = height * CameraSystem.ZOOM

        this.width = boundWidth * BOUNDING_BOX_SCALE
        this.height = boundHeight * BOUNDING_BOX_SCALE
    }

    private fun updateBounds(playerX: Float, playerY: Float) {
        val followAlpha = FOLLOW_SPEED * Gdx.graphics.deltaTime
        if (this isAbove playerY) {
            y = Interpolation.linear.apply(y, playerY, followAlpha)
        }
        if (this isBelow playerY) {
            y = Interpolation.linear.apply(y, playerY - height, followAlpha)
        }
        if (this isRight playerX) {
            x = Interpolation.linear.apply(x, playerX, followAlpha)
        }
        if (this isLeft playerX) {
            x = Interpolation.linear.apply(x, playerX - width, followAlpha)
        }
    }

    private infix fun isAbove(y: Float): Boolean {
        return this.y > y
    }

    private infix fun isBelow(y: Float): Boolean {
        return this.y + this.height < y
    }

    private infix fun isRight(x: Float): Boolean {
        return this.x > x
    }

    private infix fun isLeft(x: Float): Boolean {
        return this.x + this.width < x
    }
}