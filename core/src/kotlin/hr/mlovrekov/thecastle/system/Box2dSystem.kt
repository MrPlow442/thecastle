/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.system

import com.artemis.*
import com.artemis.annotations.*
import com.badlogic.gdx.*
import com.badlogic.gdx.maps.*
import com.badlogic.gdx.maps.objects.*
import com.badlogic.gdx.math.*
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.*
import hr.mlovrekov.thecastle.util.artemis.util.*
import hr.mlovrekov.thecastle.util.artemis.util.extension.*
import hr.mlovrekov.thecastle.util.artemis.util.system.*
import hr.mlovrekov.thecastle.util.box2d.*
import hr.mlovrekov.thecastle.physics.listener.*
import hr.mlovrekov.thecastle.util.box2d.util.*
import hr.mlovrekov.thecastle.util.box2d.util.listener.*
import hr.mlovrekov.thecastle.component.*
import hr.mlovrekov.thecastle.config.*
import hr.mlovrekov.thecastle.resource.*
import hr.mlovrekov.thecastle.util.box2d.*
import hr.mlovrekov.thecastle.util.gdx.*
import com.badlogic.gdx.utils.Array as GdxArray

/**
 * Box2d system responsible for handling game physics.
 */
@LogicSystem
class Box2dSystem : BaseEntitySystem(Aspect.all(Box2dComponent::class.java, PositionComponent::class.java)) {

    private lateinit var box2dComponentMapper: ComponentMapper<Box2dComponent>
    private lateinit var positionComponentMapper: ComponentMapper<PositionComponent>
    private lateinit var previousPositionComponentMapper: ComponentMapper<PreviousPositionComponent>

    @Wire
    private lateinit var resources: GameStateResources

    /** Box2d world/components initialization */
    val box2dWorld by lazy { World(GRAVITY, DO_SLEEP) }
    val contactListeners = ContactListenerMultiplexer(capacity = 5)

    /** Map of components and actual bodies, components are used in other systems and are used to manipulate bodies through this system  */
    private val entityBodies: IntMap<Body> = IntMap(15)

    override fun initialize() {
        //initialize lazy property
        box2dWorld

        box2dWorld.setContactListener(contactListeners)
        contactListeners += SensorContactListener()
    }

    override fun processSystem() {
        updatePreviousStates()
        box2dWorld.step(STEP_TIME, VELOCITY_ITERATIONS, POSITION_ITERATIONS)
        updateCurrentStates()
    }

    /** When an entity with a box2d component is added to the system and box2d body is created, placed on the position of  and linked to the component  */
    override fun inserted(entityId: Int) {
        val box2dComponent = box2dComponentMapper[entityId]
        val positionComponent: PositionComponent = positionComponentMapper[entityId]
        val body = createBody(box2dComponent.name, positionComponent.x, positionComponent.y, positionComponent.angle)
        val bodyUserData = body.userData as UserData
        bodyUserData["box2dComponent"] = box2dComponent

        entityBodies[entityId] = body
    }

    /** When a box2d component is removed from an entity it's body is removed from the box2d world */
    override fun removed(entityId: Int) {
        val body = entityBodies[entityId]
        box2dWorld.destroyBody(body)
        entityBodies.remove(entityId)
    }

    /**
     * Body creators
     */

    /** Creates a body from a map object */
    @Deprecated("Bodies shouldn't leave box2dSystem, this'll be replaced")
    fun createBody(mapObject: MapObject): Body {
        return when (mapObject) {
            is RectangleMapObject -> bodyFromRectangleMapObject(mapObject)
            else                  -> throw NotImplementedError("Finish this!!")
        }
    }

    private fun bodyFromRectangleMapObject(mapObject: RectangleMapObject): Body {
        val objectShape = ShapeBuilders.fromRectangleMapObject(mapObject)
        val fixtureDef = FixtureDef().apply {
            shape = objectShape
            friction = 0.5f
        }

        val bodyDef = BodyDef().apply {
            type = BodyDef.BodyType.StaticBody
            position.x = mapObject.rectangle.x * INVERSE_SCALE
            position.y = mapObject.rectangle.y * INVERSE_SCALE
        }

        val body = box2dWorld.createBody(bodyDef)
        body.createFixture(fixtureDef)

        objectShape.dispose()

        return body
    }

    /**
     * Body manipulation
     */

    /** Apply a force to the center of mass.
     * @param entityId which contains box2dComponent and is registered in Box2dSystem
     * @param forceX the world force vector, usually in Newtons (N).
     * @param forceY the world force vector, usually in Newtons (N).
     * @param wake up the body
     */
    fun applyForceToCenter(entityId: Int, forceX: Float, forceY: Float, wake: Boolean) {
        val body = entityBodies[entityId]
        if (body != null) {
            body.applyForceToCenter(forceX, forceY, wake)
        } else {
            Gdx.app.log("Box2dSystem WARNING",
                        "applyForceToCenter(forceX = $forceX, forceY = $forceY, wake = $wake) called on entity which isn't registered in box2dSystem")
        }
    }

    /** Apply a force to the center of mass.
     * @param entityId which contains box2dComponent and is registered in Box2dSystem
     * @param force the world force vector, usually in Newtons (N).
     * @param wake up the body
     */
    fun applyForceToCenter(entityId: Int, force: Vector2, wake: Boolean) {
        applyForceToCenter(entityId, force.x, force.y, wake)
    }


    /** Apply an impulse at a point. This immediately modifies the velocity. It also modifies the angular velocity if the point of
     * application is not at the center of mass.
     * @param entityId which contains box2dComponent and is registered in Box2dSystem
     * @param impulseX the world impulse vector on the x-axis, usually in N-seconds or kg-m/s.
     * @param impulseY the world impulse vector on the y-axis, usually in N-seconds or kg-m/s.
     * @param pointX the world position of the point of application on the x-axis.
     * @param pointY the world position of the point of application on the y-axis.
     * @param wake up the body
     */
    fun applyLinearImpulse(entityId: Int,
                           impulseX: Float,
                           impulseY: Float,
                           pointX: Float,
                           pointY: Float,
                           wake: Boolean) {
        val body = entityBodies[entityId]
        if (body != null) {
            body.applyLinearImpulse(impulseX, impulseY, pointX, pointY, wake)
        } else {
            Gdx.app.log("Box2dSystem WARNING",
                        "applyLinearImpulse(impulseX = $impulseX, impulseY = $impulseY, wake = $wake) called on entity which isn't registered in box2dSystem")
        }
    }

    /** Apply an impulse at a point. This immediately modifies the velocity. It also modifies the angular velocity if the point of
     * application is not at the center of mass.
     * @param entityId which contains box2dComponent and is registered in Box2dSystem
     * @param impulse the world impulse vector, usually in N-seconds or kg-m/s.
     * @param point the world position of the point of application.
     * @param wake up the body
     */
    fun applyLinearImpulse(entityId: Int, impulse: Vector2, point: Vector2, wake: Boolean) {
        applyLinearImpulse(entityId, impulse.x, impulse.y, point.x, point.y, wake)
    }


    /** Apply an impulse at center. This immediately modifies the velocity. It also modifies the angular velocity if the point of
     * application is not at the center of mass.
     * @param entityId which contains box2dComponent and is registered in Box2dSystem
     * @param impulseX the world impulse vector on the x-axis, usually in N-seconds or kg-m/s.
     * @param impulseY the world impulse vector on the y-axis, usually in N-seconds or kg-m/s.
     * @param wake up the body
     */
    fun applyLinearImpulseToCenter(entityId: Int, impulseX: Float, impulseY: Float, wake: Boolean) {
        val body = entityBodies[entityId]
        val bodyCenter = body.worldCenter
        if (body != null) {
            body.applyLinearImpulse(impulseX, impulseY, bodyCenter.x, bodyCenter.y, wake)
        } else {
            Gdx.app.log("Box2dSystem WARNING",
                        "applyLinearImpulseToCenter(impulseX = $impulseX, impulseY = $impulseY, wake = $wake) called on entity which isn't registered in box2dSystem")
        }
    }

    //FIXME: remove this
    @Deprecated("External systems/managers/whatever shouldn't have access to bodies, this'll be removed when those dependencies are removed")
    fun destroyBody(body: Body) {
        box2dWorld.destroyBody(body)
    }

    override fun dispose() {
        contactListeners.dispose()
        box2dWorld.dispose()
    }

    /**
     * Internal
     */

    /**
     *  Creates a box2d body from body template [name] and position
     *  @param name of the body template
     *  @param x position of the body on the x axis
     *  @param y position of the body on the y axis
     *  @param angle of the body
     */
    private fun createBody(name: String, x: Float, y: Float, angle: Float): Body {
        val entityData: Box2dEntityTemplate = resources[name]

        val bodyDef = entityData.body.build()
        bodyDef.position.set(x * INVERSE_SCALE, y * INVERSE_SCALE)
        bodyDef.angle = angle * MathUtils.degreesToRadians

        val body = box2dWorld.createBody(bodyDef)
        body.userData = entityData.body.userData

        entityData.fixtures.forEach {
            val fixture = body.createFixture(it.build())
            fixture.userData = it.userData
        }

        return body
    }

    private fun updatePreviousStates() {
        for ((entityId) in entityBodies) {
            updatePreviousState(entityId)
        }
    }

    private fun updatePreviousState(entityId: Int) {
        val box2dComponent = box2dComponentMapper[entityId]

        /**
         * Add a [PreviousPositionComponent] if entity doesn't yet have it, so that we can safely store previous values
         */
        val previousPositionComponent = if (entityId !in previousPositionComponentMapper) {
            world.edit(entityId).add(PreviousPositionComponent())
            previousPositionComponentMapper[entityId]
        } else {
            previousPositionComponentMapper[entityId]
        }

        previousPositionComponent.apply {
            x = box2dComponent.worldCenterX
            y = box2dComponent.worldCenterY
            angle = box2dComponent.angle
        }

    }

    private fun updateCurrentStates() {
        for ((entityId, body) in entityBodies) {
            updateCurrentState(entityId, body)
        }
    }

    private fun updateCurrentState(entityId: Int, body: Body) {
        val box2dComponent = box2dComponentMapper[entityId]

        box2dComponent.apply {
            b2PositionX = body.position.x
            b2PositionY = body.position.y
            worldCenterX = body.worldCenter.x
            worldCenterY = body.worldCenter.y
            localCenterX = body.localCenter.x
            localCenterY = body.localCenter.y

            angle = body.angle

            linearVelocityX = body.linearVelocity.x
            linearVelocityY = body.linearVelocity.y
            angularVelocity = body.angularVelocity

            mass = body.massData.mass
            centerOfMassX = body.massData.center.x
            centerOfMassY = body.massData.center.y
            I = body.massData.I

            isBullet = body.isBullet
            isActive = body.isActive

            isAwake = body.isAwake
        }
    }
}