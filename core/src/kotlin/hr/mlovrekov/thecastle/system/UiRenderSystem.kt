/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.system

import com.artemis.*
import com.artemis.annotations.*
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.scenes.scene2d.*
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.viewport.*
import hr.mlovrekov.thecastle.resource.*
import hr.mlovrekov.thecastle.util.artemis.util.system.*
import hr.mlovrekov.thecastle.util.scene2d.dsl.*

/**
 * TODO: expose api, make usable
 * TODO: Separate into debugUiSystem, UiSystem, consoleUiSystem or create subsystems or split into widgets
 */
class UiRenderSystem : BaseSystem(), ResizeAware {

    private lateinit var spriteBatchSystem: SpriteBatchSystem
    private lateinit var cameraSystem: CameraSystem

    @Wire
    private lateinit var resources: GameStateResources

    private val batch by lazy { spriteBatchSystem.guiBatch }
    private val camera by lazy { cameraSystem.guiCamera }

    private lateinit var skin: Skin

    private lateinit var guiStage: Stage

//    private lateinit var guiTable: Table

    override fun initialize() {
        skin = skin {
            properties["default-font"] = resources.get<BitmapFont>("Gui-Default.ttf")
            properties["default"] = labelStyle {
                font = resources["Gui-Default.ttf"]
                fontColor = Color.LIGHT_GRAY
            }
            properties["small"] = labelStyle {
                font = resources["Gui-Small.ttf"]
                fontColor = Color.LIGHT_GRAY
            }
            properties["tiny"] = labelStyle {
                font = resources["Gui-Tiny.ttf"]
                fontColor = Color.LIGHT_GRAY
            }
            properties["large"] = labelStyle {
                font = resources["Gui-Large.ttf"]
                fontColor = Color.LIGHT_GRAY
            }
            properties["default"] = windowStyle {
                titleFont = resources["Gui-Small.ttf"]
            }
        }

        guiStage = Stage(ScreenViewport(camera), batch)
        guiStage.setDebugAll(true)
        guiStage.setDebugInvisible(true)

//        guiStage.addActor(debugDisplay)

//        guiTable = Table(skin)
//        guiTable.setFillParent(true)
//        guiTable.align(Align.topRight)
//        guiTable.add(Label("Test", skin))
//
//        guiStage.addActor(guiTable)

        println("ACTORS ${guiStage.actors}")
    }

    override fun processSystem() {
        guiStage.act(world.delta)
        guiStage.draw()
    }

    override fun onResize(width: Int, height: Int) {
        guiStage.viewport.update(width, height, true)
    }

    override fun dispose() {
        guiStage.dispose()
    }

}