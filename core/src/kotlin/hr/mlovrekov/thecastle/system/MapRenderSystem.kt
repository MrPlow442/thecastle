/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.system

import com.artemis.*
import com.badlogic.gdx.maps.tiled.renderers.*

class MapRenderSystem : BaseSystem() {

    private lateinit var spriteBatchSystem: SpriteBatchSystem
    private lateinit var mapSystem: MapSystem
    private lateinit var cameraSystem: CameraSystem

    private val batch by lazy { spriteBatchSystem.mainBatch }
    private val mapRenderer by lazy { OrthogonalTiledMapRenderer(mapSystem.map, batch) }

    override fun begin() {
        batch.projectionMatrix = cameraSystem.mainCamera.combined
    }

    override fun processSystem() {
        mapRenderer.setView(cameraSystem.mainCamera)
        mapRenderer.render()
    }

}
