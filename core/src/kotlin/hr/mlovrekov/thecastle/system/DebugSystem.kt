/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.system

import com.artemis.*
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.glutils.*
import com.badlogic.gdx.math.*
import com.badlogic.gdx.physics.box2d.*
import hr.mlovrekov.thecastle.component.*
import hr.mlovrekov.thecastle.config.*
import hr.mlovrekov.thecastle.util.artemis.util.extension.*
import hr.mlovrekov.thecastle.util.debug.*
import hr.mlovrekov.thecastle.util.gdx.*
import hr.mlovrekov.thecastle.util.locator.*
import com.badlogic.gdx.utils.Array as GdxArray

class DebugSystem : BaseEntitySystem(Aspect.one(PlayerComponent::class.java,
                                                PositionComponent::class.java,
                                                Box2dComponent::class.java,
                                                ActionComponent::class.java)) {

    private lateinit var cameraSystem: CameraSystem
    private lateinit var box2dSystem: Box2dSystem
    private lateinit var playerCameraFollowSystem: PlayerCameraFollowSystem

    private lateinit var uiRenderSystem: UiRenderSystem

    private lateinit var playerComponentMapper: ComponentMapper<PlayerComponent>
    private lateinit var positionComponentMapper: ComponentMapper<PositionComponent>
    private lateinit var previousPositionComponentMapper: ComponentMapper<PreviousPositionComponent>
    private lateinit var animationComponentMapper: ComponentMapper<AnimationComponent>
    private lateinit var box2dComponentMapper: ComponentMapper<Box2dComponent>
    private lateinit var actionComponentMapper: ComponentMapper<ActionComponent>

    val entities = GdxArray<Int>()

    private val mainCamera by lazy { cameraSystem.mainCamera }

    val box2dDebugRenderer = Box2DDebugRenderer()
    val shapeRenderer = ShapeRenderer()

    override fun initialize() {
        isEnabled = ServiceLocator.preferences.getBoolean("debug")
    }

    override fun processSystem() {
        setPlayerDebugInfo()
        renderRenderPosition()
        box2dDebugRenderer.render(box2dSystem.box2dWorld, cameraSystem.box2DDebugCamera.combined)
        renderCameraBounds()
    }

    override fun inserted(entityId: Int) {
        entities += entityId
    }

    override fun removed(entityId: Int) {
        entities -= entityId
    }

    private fun setPlayerDebugInfo() {
        val entityId = entities.find {
            it in playerComponentMapper &&
            it in positionComponentMapper &&
            it in box2dComponentMapper &&
            it in actionComponentMapper
        } ?: return

        val positionComponent = positionComponentMapper[entityId]
        val previousPositionComponent = previousPositionComponentMapper.getSafe(entityId)
        val box2dComponent = box2dComponentMapper[entityId]
        val actionComponent = actionComponentMapper[entityId]

//        uiRenderSystem.setDebugLabel("Velocity", "Vel", "x: ${box2dComponent.linearVelocityX}, y: ${box2dComponent.linearVelocityY}, angularVelocity: ${box2dComponent.angularVelocity}")
//        uiRenderSystem.setDebugLabel("VelocityTitle", "Velocity", true)
//        uiRenderSystem.setDebugLabel("VelocityText", "x: ${box2dComponent.linearVelocityX}, y: ${box2dComponent.linearVelocityY}, angularVelocity: ${box2dComponent.angularVelocity}", false)
        OnScreenLogger["Vel"] = "x: ${box2dComponent.linearVelocityX}, y: ${box2dComponent.linearVelocityY}, angularVelocity: ${box2dComponent.angularVelocity}"
        if(previousPositionComponent != null) {
            OnScreenLogger["PrevPos"] = "x: ${previousPositionComponent.x}, y: ${previousPositionComponent.y}, rot: ${previousPositionComponent.angle}"
        }
        OnScreenLogger["Pos"] = "x: ${box2dComponent.worldCenterX * SCALE}, y: ${box2dComponent.worldCenterY * SCALE}, rot: ${box2dComponent.angle * MathUtils.radiansToDegrees}"
        OnScreenLogger["LerpPos"] = "x: ${positionComponent.x}, y: ${positionComponent.y}, rot: ${positionComponent.angle}"
        OnScreenLogger["Action"] = "${actionComponent.currentAction.name}"
        OnScreenLogger["Grounded"] = "contacts: ${box2dComponent.footContacts}, grounded: ${box2dComponent.isGrounded}"
    }

    private fun renderRenderPosition() {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        shapeRenderer.projectionMatrix = cameraSystem.mainCamera.combined
        for (entityId in entities) {

            if (entityId !in positionComponentMapper ||
                entityId !in animationComponentMapper) {
                continue
            }

            val positionComponent = positionComponentMapper[entityId]

            shapeRenderer.color = Color.RED
            shapeRenderer.rect(positionComponent.x, positionComponent.y, 1f, 1f)
        }
        shapeRenderer.end()
    }

    private fun renderCameraBounds() {
        shapeRenderer.projectionMatrix = mainCamera.combined
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
        shapeRenderer.color = Color.WHITE
        shapeRenderer.rect(playerCameraFollowSystem.x,
                           playerCameraFollowSystem.y,
                           playerCameraFollowSystem.width,
                           playerCameraFollowSystem.height)
        shapeRenderer.end()
    }

}