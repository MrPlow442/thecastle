/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.system

import com.artemis.*
import com.artemis.systems.*
import hr.mlovrekov.thecastle.util.artemis.util.*
import hr.mlovrekov.thecastle.util.artemis.util.extension.*
import hr.mlovrekov.thecastle.component.*
import hr.mlovrekov.thecastle.component.ActionComponent.*

class ActionSystem : IteratingSystem(Aspect.all(ActionComponent::class.java).one(Box2dComponent::class.java)) {

    private lateinit var actionComponentMapper: ComponentMapper<ActionComponent>
    private lateinit var box2dComponentMapper: ComponentMapper<Box2dComponent>

    override fun process(entityId: Int) {
        if (entityId in box2dComponentMapper) {
            processBox2dComponent(entityId)
        }
    }

    fun processBox2dComponent(entityId: Int) {
        val actionComponent = actionComponentMapper[entityId]
        val box2dComponent = box2dComponentMapper[entityId]

        val movingRight = box2dComponent.linearVelocityX > 0f
        val movingLeft = box2dComponent.linearVelocityX < 0f
        val jumping = box2dComponent.linearVelocityY > 0f

        if (box2dComponent.isGrounded) {
            if (movingRight) {
                actionComponent.currentAction = Action.MOVING_RIGHT
            } else if (movingLeft) {
                actionComponent.currentAction = Action.MOVING_LEFT
            } else {
                actionComponent.currentAction = Action.DEFAULT
            }
        } else {
            if (jumping) {
                if (movingRight) {
                    actionComponent.currentAction = Action.JUMPING_RIGHT
                } else if (movingLeft) {
                    actionComponent.currentAction = Action.JUMPING_LEFT
                } else {
                    actionComponent.currentAction = Action.JUMPING
                }
            } else {
                if (movingRight) {
                    actionComponent.currentAction = Action.FALLING_RIGHT
                } else if (movingLeft) {
                    actionComponent.currentAction = Action.FALLING_LEFT
                } else {
                    actionComponent.currentAction = Action.FALLING
                }
            }
        }
    }
}