/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.system

import com.artemis.*
import com.badlogic.gdx.graphics.*
import hr.mlovrekov.thecastle.config.*

class CameraSystem : BaseSystem() {
    companion object {
        const val ZOOM: Float = 0.5f
    }

    val mainCamera: OrthographicCamera by lazy { OrthographicCamera(WIDTHF * ZOOM, HEIGHTF * ZOOM) }
    val guiCamera: OrthographicCamera by lazy { OrthographicCamera(WIDTHF, HEIGHTF) }
    val box2DDebugCamera: OrthographicCamera by lazy {
        OrthographicCamera(WIDTHF * ZOOM * INVERSE_SCALE,
                           HEIGHTF * ZOOM * INVERSE_SCALE)
    }

    override fun initialize() {
        mainCamera.setToOrtho(false, WIDTHF * ZOOM, HEIGHTF * ZOOM)
        mainCamera.update()

        guiCamera.setToOrtho(false, WIDTHF, HEIGHTF)
        guiCamera.update()

        box2DDebugCamera.setToOrtho(false, WIDTHF * ZOOM * INVERSE_SCALE, HEIGHTF * ZOOM * INVERSE_SCALE)
        box2DDebugCamera.update()
    }

    override fun processSystem() {
        mainCamera.update()
        guiCamera.update()
        box2DDebugCamera.update()
    }

    fun setCameraPosition(camera: OrthographicCamera,
                          x: Float = 0f,
                          y: Float = 0f,
                          z: Float = 0f,
                          xOffset: Float = 0f,
                          yOffset: Float = 0f,
                          zOffset: Float = 0f,
                          scale: Boolean = false) {
        var xPos = (x + xOffset)
        var yPos = (y + yOffset)
        var zPos = (z + zOffset)

        if (scale) {
            xPos *= INVERSE_SCALE
            yPos *= INVERSE_SCALE
            zPos *= INVERSE_SCALE
        }

        camera.position.x = xPos
        camera.position.y = yPos
        camera.position.z = zPos
    }

    fun setMainCameraPosition(x: Float = 0f,
                              y: Float = 0f,
                              z: Float = 0f,
                              xOffset: Float = 0f,
                              yOffset: Float = 0f,
                              zOffset: Float = 0f) {
        setCameraPosition(mainCamera, x, y, z, xOffset, yOffset, zOffset)
    }

    fun setBox2dDebugCameraPosition(x: Float = 0f,
                                    y: Float = 0f,
                                    z: Float = 0f,
                                    xOffset: Float = 0f,
                                    yOffset: Float = 0f,
                                    zOffset: Float = 0f) {
        setCameraPosition(box2DDebugCamera, x, y, z, xOffset, yOffset, zOffset, true)
    }

    fun setGuiCameraPosition(x: Float = 0f,
                             y: Float = 0f,
                             z: Float = 0f,
                             xOffset: Float = 0f,
                             yOffset: Float = 0f,
                             zOffset: Float = 0f) {
        setCameraPosition(guiCamera, x, y, z, xOffset, yOffset, zOffset)
    }
}