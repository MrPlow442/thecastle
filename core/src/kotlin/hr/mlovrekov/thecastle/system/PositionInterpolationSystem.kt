/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.system

import com.artemis.*
import com.artemis.systems.*
import com.badlogic.gdx.math.*
import hr.mlovrekov.thecastle.component.*
import hr.mlovrekov.thecastle.config.*

class PositionInterpolationSystem : IteratingSystem(Aspect.all(Box2dComponent::class.java, PreviousPositionComponent::class.java, PositionComponent::class.java)) {

    private lateinit var box2dComponentMapper: ComponentMapper<Box2dComponent>
    private lateinit var previousPositionComponentMapper: ComponentMapper<PreviousPositionComponent>
    private lateinit var positionComponentMapper: ComponentMapper<PositionComponent>

    override fun process(entityId: Int) {
        interpolateState(entityId, world.delta)
    }

    private fun interpolateState(entityId: Int, alpha: Float) {
        val box2dComponent = box2dComponentMapper[entityId]
        val previousPositionComponent = previousPositionComponentMapper[entityId]
        val positionComponent = positionComponentMapper[entityId]

        val interpolatedB2dX = Interpolation.linear.apply(previousPositionComponent.x, box2dComponent.worldCenterX, alpha)
        val interpolatedB2dY = Interpolation.linear.apply(previousPositionComponent.y, box2dComponent.worldCenterY, alpha)
        val interpolatedB2dAngle = Interpolation.linear.apply(previousPositionComponent.angle, box2dComponent.angle, alpha)

        positionComponent.apply {
            x = interpolatedB2dX * SCALE
            y = interpolatedB2dY * SCALE
            angle = interpolatedB2dAngle * MathUtils.radiansToDegrees
        }

    }
}