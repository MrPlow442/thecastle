/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.system

import com.artemis.*
import com.artemis.annotations.*
import hr.mlovrekov.thecastle.command.*
import hr.mlovrekov.thecastle.component.*
import hr.mlovrekov.thecastle.input.*
import hr.mlovrekov.thecastle.resource.*
import com.badlogic.gdx.utils.Array as GdxArray

class PlayerInputSystem : BaseEntitySystem(Aspect.all(PlayerComponent::class.java)), InputHandler {

    private lateinit var commandQueueSystem: CommandQueueSystem

    private val playerEntityId by lazy { subscription.entities.get(0) }

    @Wire
    private lateinit var resources: GameStateResources

    override fun processSystem() {}

    override fun inputActive(key: Int): Boolean {
        when(key) {
            InputController.Inputs.LEFT  -> commandQueueSystem.enqueue(resources.get<Command>("playerMoveLeft.cmd") toEntity playerEntityId)
            InputController.Inputs.RIGHT -> commandQueueSystem.enqueue(resources.get<Command>("playerMoveRight.cmd") toEntity playerEntityId)
            else                             -> return false
        }
        return true
    }

    override fun inputActivated(key: Int): Boolean {
        when(key) {
            InputController.Inputs.JUMP -> commandQueueSystem.enqueue(resources.get<Command>("playerJump.cmd") toEntity playerEntityId)
            else                            -> return false
        }
        return true
    }

    override fun inputDeactivated(key: Int): Boolean {
        return true
    }
}

