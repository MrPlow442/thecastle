/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.state

import com.artemis.*
import com.artemis.utils.*
import hr.mlovrekov.thecastle.game.strategy.*
import hr.mlovrekov.thecastle.input.*
import hr.mlovrekov.thecastle.resource.*
import hr.mlovrekov.thecastle.system.*
import hr.mlovrekov.thecastle.util.artemis.util.extension.*
import hr.mlovrekov.thecastle.util.artemis.util.system.*
import hr.mlovrekov.thecastle.util.locator.*
import hr.mlovrekov.thecastle.util.state.*
import com.badlogic.gdx.utils.Array as GdxArray

class GameState(val resources: Resources) : State() {

    private lateinit var world: World

    private val inputHandlers = GdxArray<InputHandler>(4)

    override fun create() {

        val worldConfig = WorldConfigurationBuilder()
                .with(CommandQueueSystem(),
                      Box2dSystem(),
                      PositionInterpolationSystem(),
                      PlayerInputSystem(),
                      MapSystem(),
                      ActionSystem(),
                      CameraSystem(),
                      SpriteBatchSystem(),
                      MapRenderSystem(),
                      AnimationRenderSystem(),
                      UiRenderSystem(),
                      ShaderSystem(),
                      PlayerCameraFollowSystem(),
                      DebugSystem())
                .build()
                .setInvocationStrategy(FixedTimestepInvocationStrategy())
                .register(resources)
                .register(ServiceLocator.inputController)

        world = World(worldConfig)

        registerInputHandlers(world.systems)

        // TEMP
        world.createEntityFromEntityTemplate(resources["playerEntity"])
    }

    override fun render(delta: Float) {
        world.delta = delta
        world.process()
    }

    override fun resize(width: Int, height: Int) {
        world.systems.forEach {
            if (it is ResizeAware) {
                it.onResize(width, height)
            }
        }
    }

    override fun dispose() {
        removeInputHandlers()
        world.dispose()
        resources.dispose()
    }

    private fun registerInputHandlers(systems: ImmutableBag<BaseSystem>) {
        for(system in systems) {
            if(system is InputHandler) {
                inputHandlers.add(system)
                ServiceLocator.inputController.addHandler(system)
            }
        }
    }

    private fun removeInputHandlers() {
        for(handler in inputHandlers) {
            ServiceLocator.inputController.removeHandler(handler)
        }
    }
}