/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.state

import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.freetype.*
import com.badlogic.gdx.scenes.scene2d.*
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.*
import com.badlogic.gdx.utils.viewport.*
import hr.mlovrekov.thecastle.config.*
import hr.mlovrekov.thecastle.util.state.*
import hr.mlovrekov.thecastle.util.widget.console.*

class UiTestState : State() {

    private lateinit var skin: Skin
    private lateinit var guiStage: Stage
    val guiCamera: OrthographicCamera by lazy { OrthographicCamera(WIDTHF, HEIGHTF) }

    override fun create() {
        guiCamera.setToOrtho(false, WIDTHF, HEIGHTF)
        guiCamera.update()

        guiStage = Stage(ScreenViewport(guiCamera))
        guiStage.setDebugAll(true)
        guiStage.setDebugInvisible(true)

        val fontGenerator = FreeTypeFontGenerator(Gdx.files.internal("Early Gameboy.ttf"))
        val defaultFont = fontGenerator.generateFont(FreeTypeFontGenerator.FreeTypeFontParameter().apply { size = 16 })
        val smallFont = fontGenerator.generateFont(FreeTypeFontGenerator.FreeTypeFontParameter().apply { size = 10 })
        val tinyFont = fontGenerator.generateFont(FreeTypeFontGenerator.FreeTypeFontParameter().apply { size = 5 })
        val largeFont = fontGenerator.generateFont(FreeTypeFontGenerator.FreeTypeFontParameter().apply { size = 22 })

        skin = Skin(Gdx.files.internal("ui/uiskin.json"))

//        skin = skin {
//            properties["default-font"] = defaultFont
//            properties["default"] = labelStyle {
//                font = defaultFont
//                fontColor = Color.LIGHT_GRAY
//            }
//            properties["small"] = labelStyle {
//                font = smallFont
//                fontColor = Color.LIGHT_GRAY
//            }
//            properties["tiny"] = labelStyle {
//                font = tinyFont
//                fontColor = Color.LIGHT_GRAY
//            }
//            properties["large"] = labelStyle {
//                font = largeFont
//                fontColor = Color.LIGHT_GRAY
//            }
//            properties["default"] = windowStyle {
//                titleFont = smallFont
//            }
//            properties["default"] = textFieldStyle {
//                font = smallFont
//                fontColor = Color.LIGHT_GRAY
//            }
//        }

//        guiStage.addActor(Label("Test", skin))

        guiStage.addActor(Table(skin).apply {
            setFillParent(true)
            align(Align.topLeft)
            defaults().expand(true, false)
            add(Table(skin).apply {
                add("HP:")
                add("100")
                add("").pad(0f, 5f, 0f, 5f)
                add("MP:")
                add("100")
            }).align(Align.left)
            add("")
            add("")
            add("LabelSpaced")
            row()
            add(Table(skin).apply {
                add("Nested1")
                add("Nested2")
                add("Nested3")
                row()
                add("Nested4")
                add("Nested5")
                add("Nested6")
            })
            row()
            add("Label4")
            add("Label5")
            add("Label6")
        })

        val console = Console(skin)

        guiStage.addActor(console)

        Gdx.input.inputProcessor = guiStage
    }

    override fun render(delta: Float) {
        guiCamera.update()
        guiStage.act(delta)
        guiStage.draw()
    }
}