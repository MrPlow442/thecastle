/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.state

import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.*
import hr.mlovrekov.thecastle.input.*
import hr.mlovrekov.thecastle.resource.*
import hr.mlovrekov.thecastle.util.locator.*
import hr.mlovrekov.thecastle.util.state.*

class TestState() : State() {
    private val batch by lazy { SpriteBatch() }
    private val texture by lazy { Texture("badlogic.jpg") }

    private val testInputHandler = TestInputHandler()

    override fun create() {
        ServiceLocator.inputController.addHandler(testInputHandler)
    }

    override fun render(delta: Float) {
        batch.begin()
        batch.draw(texture, 100f, 100f)
        batch.end()
    }

    override fun dispose() {
        ServiceLocator.inputController.removeHandler(testInputHandler)
    }

    private inner class TestInputHandler : InputHandler {

        override fun inputActive(key: Int): Boolean {
            return false
        }

        override fun inputActivated(key: Int): Boolean {
            when (key) {
                InputController.Inputs.ACCEPT ->
                    stateManager.swapWith(LoadingState(GameStateResources(),
                                                       { stateManager, resources ->
                                                           stateManager.swapWith(GameState(resources))
                                                       }))
                else                              -> return false
            }
            return true
        }

        override fun inputDeactivated(key: Int): Boolean {
            return false
        }

    }
}