/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.state

import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.scenes.scene2d.*
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.*
import com.badlogic.gdx.utils.viewport.*
import hr.mlovrekov.thecastle.resource.*
import hr.mlovrekov.thecastle.util.state.*

open class LoadingState(val resources: Resources,
                        val afterLoadFinish: (StateManager, Resources) -> Unit) : State() {

    private val guiStage by lazy { Stage(ScreenViewport()) }
    private val guiSkin by lazy { Skin() }
    private val loadingLabel by lazy { Label("Loading", guiSkin) }
    private val loadingValue by lazy { Label("0%", guiSkin) }

    private val loadingFont = BitmapFont().apply { color.set(Color.WHITE) }

    override fun create() {
        val lightGrey = Color(0.9f, 0.9f, 0.9f, 1.0f)

        guiSkin.add("default-font", loadingFont)
        guiSkin.add("default", Label.LabelStyle(loadingFont, lightGrey))

        val guiTable = Table(guiSkin)
        guiTable.setFillParent(true)
        guiTable.align(Align.center)
        guiStage.addActor(guiTable)
//        guiTable.debug = true

        guiTable.add(loadingLabel).padRight(10f)
        guiTable.add(loadingValue)

        resources.initialize()
    }

    override fun render(delta: Float) {
        val progress = (resources.progress * 100).toInt()
        loadingValue.setText("$progress%")

        guiStage.act(delta)
        guiStage.draw()

        if(resources.update()) {
            afterLoadFinish(stateManager, resources)
        }
    }

    override fun resize(width: Int, height: Int) {
        guiStage.viewport.update(width, height, true)
    }

    override fun dispose() {
        guiStage.dispose()
        loadingFont.dispose()
    }
}