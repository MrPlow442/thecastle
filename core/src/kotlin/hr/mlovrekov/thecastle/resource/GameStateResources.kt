/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.resource

import com.badlogic.gdx.assets.*
import com.badlogic.gdx.assets.loaders.resolvers.*
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.graphics.g2d.freetype.*
import com.badlogic.gdx.graphics.glutils.*
import com.badlogic.gdx.maps.tiled.*
import com.badlogic.gdx.physics.box2d.*
import hr.mlovrekov.thecastle.command.*
import hr.mlovrekov.thecastle.component.*
import hr.mlovrekov.thecastle.config.*
import hr.mlovrekov.thecastle.resource.loader.*
import hr.mlovrekov.thecastle.resource.resolver.*
import hr.mlovrekov.thecastle.util.animation.*
import hr.mlovrekov.thecastle.util.artemis.*
import hr.mlovrekov.thecastle.util.artemis.dsl.*
import hr.mlovrekov.thecastle.util.box2d.*
import hr.mlovrekov.thecastle.util.box2d.util.dsl.*

class GameStateResources : Resources {

    private val assetManager = AssetManager()

    override val progress: Float
        get() = assetManager.progress

    override fun initialize() {
        val internalFileHandleResolver = InternalFileHandleResolver()
        val nullResolver = NullResolver()

        assetManager.setLoader(FreeTypeFontGenerator::class.java,
                               FreeTypeFontGeneratorLoader(internalFileHandleResolver))
        assetManager.setLoader(BitmapFont::class.java, ".ttf", FreetypeFontLoader(internalFileHandleResolver))
        assetManager.setLoader(TiledMap::class.java, TmxMapLoader(internalFileHandleResolver))
        assetManager.setLoader(Animation::class.java, ".anim", AnimationLoader(internalFileHandleResolver))
        assetManager.setLoader(ShaderProgram::class.java, ShaderLoader(internalFileHandleResolver))
        assetManager.setLoader(Box2dEntityTemplate::class.java, Box2dEntityTemplateLoader(nullResolver))
        assetManager.setLoader(EntityTemplate::class.java, EntityTemplateLoader(nullResolver))
        assetManager.setLoader(Command::class.java, ".cmd", CommandLoader(nullResolver))
        assetManager.setLoader(AnimationPackage::class.java, ".apg", AnimationPackageLoader(nullResolver))

        // FONTS
        assetManager.load("Gui-Default.ttf",
                          BitmapFont::class.java,
                          FreetypeFontLoader.FreeTypeFontLoaderParameter().apply {
                              fontFileName = "Early Gameboy.ttf"
                              fontParameters = FreeTypeFontGenerator.FreeTypeFontParameter().apply {
                                  size = 16
                              }
                          })

        assetManager.load("Gui-Small.ttf",
                          BitmapFont::class.java,
                          FreetypeFontLoader.FreeTypeFontLoaderParameter().apply {
                              fontFileName = "Early Gameboy.ttf"
                              fontParameters = FreeTypeFontGenerator.FreeTypeFontParameter().apply {
                                  size = 10
                              }
                          })

        assetManager.load("Gui-Tiny.ttf",
                          BitmapFont::class.java,
                          FreetypeFontLoader.FreeTypeFontLoaderParameter().apply {
                              fontFileName = "Early Gameboy.ttf"
                              fontParameters = FreeTypeFontGenerator.FreeTypeFontParameter().apply {
                                  size = 5
                              }
                          })

        assetManager.load("Gui-Large.ttf",
                          BitmapFont::class.java,
                          FreetypeFontLoader.FreeTypeFontLoaderParameter().apply {
                              fontFileName = "Early Gameboy.ttf"
                              fontParameters = FreeTypeFontGenerator.FreeTypeFontParameter().apply {
                                  size = 22
                              }
                          })

        // TEXTURES
        assetManager.load("simplePlayer.png", Texture::class.java)

        // ANIMATIONS
        assetManager.load("playerIdle.anim",
                          Animation::class.java,
                          AnimationLoader.AnimationParameter(textureName = "simplePlayer.png",
                                                             startX = 0,
                                                             startY = 0,
                                                             width = 32,
                                                             height = 32,
                                                             repeatX = 1))
        assetManager.load("playerWalk.anim",
                          Animation::class.java,
                          AnimationLoader.AnimationParameter(textureName = "simplePlayer.png",
                                                             startX = 32,
                                                             startY = 0,
                                                             width = 32,
                                                             height = 32,
                                                             repeatX = 3))

        assetManager.load("playerAnim.apg",
                          AnimationPackage::class.java,
                          AnimationPackageLoader.AnimationPackageParameter(ActionComponent.Action.MOVING_LEFT to "playerWalk.anim",
                                                                           ActionComponent.Action.MOVING_RIGHT to "playerWalk.anim",
                                                                           ActionComponent.Action.JUMPING to "playerIdle.anim",
                                                                           ActionComponent.Action.JUMPING_LEFT to "playerIdle.anim",
                                                                           ActionComponent.Action.JUMPING_RIGHT to "playerIdle.anim",
                                                                           ActionComponent.Action.FALLING to "playerIdle.anim",
                                                                           ActionComponent.Action.FALLING_LEFT to "playerIdle.anim",
                                                                           ActionComponent.Action.FALLING_RIGHT to "playerIdle.anim",
                                                                           defaultAnimation = "playerIdle.anim"))

        // MAPS
        assetManager.load("rooms/testroom.tmx", TiledMap::class.java)

        // SHADERS
        assetManager.load("shader/scanline", ShaderProgram::class.java)

        // COMMANDS
        assetManager.load("playerMoveRight.cmd",
                          Command::class.java,
                          CommandLoader.CommandParameter(MoveCommand(MoveCommand.Direction.RIGHT,
                                                                     desiredVelocity = 2f)))

        assetManager.load("playerMoveLeft.cmd",
                          Command::class.java,
                          CommandLoader.CommandParameter(MoveCommand(MoveCommand.Direction.LEFT,
                                                                     desiredVelocity = 2f)))

        assetManager.load("playerJump.cmd",
                          Command::class.java,
                          CommandLoader.CommandParameter(JumpCommand(jumpForce = 250f)))

        // BOX2D TEMPLATES
        assetManager.load("player",
                          Box2dEntityTemplate::class.java,
                          Box2dEntityTemplateLoader.Box2dEntityTemplateParameter(box2dEntityTemplate {
                              bodyTemplate {
                                  type = BodyDef.BodyType.DynamicBody
                                  fixedRotation = true
                                  userData {
                                      set("name" to "player")
                                  }
                              }
                              fixtureTemplate {
                                  polygonShape {
                                      setAsBox(8.0f * INVERSE_SCALE,
                                               10.0f * INVERSE_SCALE)
                                  }
                                  friction = 0.0f
                                  userData {
                                      set("name" to "upperBody")
                                  }
                              }
                              fixtureTemplate {
                                  circleShape {
                                      radius = 8.0f * INVERSE_SCALE
                                      position(0.0f * INVERSE_SCALE,
                                               -10.0f * INVERSE_SCALE)
                                  }
                                  friction = 0.2f
                                  userData {
                                      set("name" to "lowerBody")
                                  }
                              }
                              fixtureTemplate {
                                  polygonShape {
                                      setAsBox(7.0f * INVERSE_SCALE,
                                               2.0f * INVERSE_SCALE,
                                               center(0.0f * INVERSE_SCALE,
                                                      -16.0f * INVERSE_SCALE),
                                               0.0f)
                                  }
                                  friction = 0.0f
                                  isSensor = true
                                  userData {
                                      set("name" to "foot")
                                  }
                              }
                          }))

        // ECS ENTITY TEMPLATES
        assetManager.load("playerEntity",
                          EntityTemplate::class.java,
                          EntityTemplateLoader.EntityTemplateParameter(entity {
                              component(PlayerComponent::class.java)
                              component(ActionComponent::class.java)
                              component(Box2dComponent::class.java) {
                                  name = "player"
                              }
                              component(PositionComponent::class.java) {
                                  x = 150f
                                  y = 200f
                              }
                              component(AnimationComponent::class.java) {
                                  layer = AnimationComponent.Layer.DEFAULT
                                  animationPackageName = "playerAnim.apg"
                              }
                          }))
    }

    override operator fun <T : Any> get(fileName: String): T = assetManager.get(fileName)

    override fun update(): Boolean = assetManager.update()

    override fun dispose(): Unit {
        assetManager.dispose()
    }
}