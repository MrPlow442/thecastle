/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.resource.loader

import com.badlogic.gdx.assets.*
import com.badlogic.gdx.assets.loaders.*
import com.badlogic.gdx.files.*
import com.badlogic.gdx.utils.Array
import hr.mlovrekov.thecastle.command.*

class CommandLoader(resolver: FileHandleResolver) :
        AsynchronousAssetLoader<Command, CommandLoader.CommandParameter>(resolver) {

    private var command: Command? = null

    override fun loadAsync(manager: AssetManager, fileName: String, file: FileHandle?, parameter: CommandParameter) {
        command = parameter.command
    }

    override fun loadSync(manager: AssetManager,
                          fileName: String,
                          file: FileHandle?,
                          parameter: CommandParameter): Command? {
        val command = this.command
        this.command = null
        return command
    }

    override fun getDependencies(fileName: String,
                                 file: FileHandle?,
                                 parameter: CommandParameter?): Array<AssetDescriptor<Any>>? {
        return null
    }

    class CommandParameter(val command: Command) : AssetLoaderParameters<Command>()
}