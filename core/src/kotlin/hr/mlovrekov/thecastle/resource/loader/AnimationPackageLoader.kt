/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.resource.loader

import com.badlogic.gdx.assets.*
import com.badlogic.gdx.assets.loaders.*
import com.badlogic.gdx.files.*
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.utils.Array
import hr.mlovrekov.thecastle.component.*
import hr.mlovrekov.thecastle.util.animation.*
import java.util.*
import com.badlogic.gdx.utils.Array as GdxArray

class AnimationPackageLoader(resolver: FileHandleResolver?) : AsynchronousAssetLoader<AnimationPackage, AnimationPackageLoader.AnimationPackageParameter>(
        resolver) {

    override fun loadAsync(manager: AssetManager,
                           fileName: String,
                           file: FileHandle?,
                           parameter: AnimationPackageParameter?) {
    }

    override fun loadSync(manager: AssetManager,
                          fileName: String,
                          file: FileHandle?,
                          parameter: AnimationPackageParameter?): AnimationPackage? {

        val actionAnimationNames = parameter?.getAll()
        if (actionAnimationNames != null) {
            // map animation names to their animation counterparts and add them to the animation package
            val actionAimations = actionAnimationNames.mapValuesTo(HashMap<ActionComponent.Action, Animation>(
                    actionAnimationNames.size), { manager.get(it.value, Animation::class.java) })

            return AnimationPackage(actionAimations)
        } else {
            // we've got no parameter so we assume that the filename is the name of the animation
            return AnimationPackage(manager.get(fileName, Animation::class.java))
        }
    }

    override fun getDependencies(fileName: String,
                                 file: FileHandle?,
                                 parameter: AnimationPackageParameter?): Array<AssetDescriptor<out Any>> {
        val actionAnimationNames = parameter?.getAll()
        val dependencies = GdxArray<AssetDescriptor<out Any>>(actionAnimationNames?.size ?: 1)

        if (actionAnimationNames != null) {
            for ((action, animationName) in actionAnimationNames) {
                dependencies.add(AssetDescriptor(animationName, Animation::class.java))
            }
        } else {
            dependencies.add(AssetDescriptor(fileName, Animation::class.java))
        }

        return dependencies
    }


    class AnimationPackageParameter(private val actionAnimationNames: HashMap<ActionComponent.Action, String>,
                                    private val defaultAnimationName: String) : AssetLoaderParameters<AnimationPackage>() {
        init {
            actionAnimationNames[ActionComponent.Action.DEFAULT] = defaultAnimationName
        }

        constructor(vararg pairs: Pair<ActionComponent.Action, String>, defaultAnimation: String)
        : this(HashMap<ActionComponent.Action, String>(pairs.size).apply { putAll(pairs) }, defaultAnimation)

        fun getAll(): Map<ActionComponent.Action, String> = actionAnimationNames

        fun getDefault(): String = actionAnimationNames[ActionComponent.Action.DEFAULT]!!
    }
}