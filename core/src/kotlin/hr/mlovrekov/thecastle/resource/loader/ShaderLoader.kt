/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.resource.loader

import com.badlogic.gdx.assets.*
import com.badlogic.gdx.assets.loaders.*
import com.badlogic.gdx.files.*
import com.badlogic.gdx.graphics.glutils.*
import com.badlogic.gdx.utils.Array

class ShaderLoader(val resolver: FileHandleResolver) : AsynchronousAssetLoader<ShaderProgram, ShaderLoader.ShaderParameter>(
        resolver) {

    companion object {
        private val DEFAULT_PARAMETER = ShaderParameter()
    }

    override fun loadAsync(manager: AssetManager?, fileName: String?, file: FileHandle?, parameter: ShaderParameter?) {}

    override fun loadSync(manager: AssetManager,
                          fileName: String,
                          file: FileHandle?,
                          parameter: ShaderParameter?): ShaderProgram? {
        val param = parameter ?: DEFAULT_PARAMETER

        val vertexFileHandle = resolver.resolve("${param.vertexPath ?: fileName}${param.vertexExtension}")
        val fragmentFileHandle = resolver.resolve("${param.fragmentPath ?: fileName}${param.fragmentExtension}")

        return ShaderProgram(vertexFileHandle, fragmentFileHandle)
    }

    override fun getDependencies(fileName: String?,
                                 file: FileHandle?,
                                 parameter: ShaderParameter?): Array<AssetDescriptor<Any>>? = null

    class ShaderParameter(val vertexPath: String? = null,
                          val fragmentPath: String? = null,
                          val vertexExtension: String = ".vert",
                          val fragmentExtension: String = ".frag") : AssetLoaderParameters<ShaderProgram>()
}