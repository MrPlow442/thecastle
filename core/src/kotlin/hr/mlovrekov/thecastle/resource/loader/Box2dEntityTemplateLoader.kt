/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.resource.loader

import com.badlogic.gdx.assets.*
import com.badlogic.gdx.assets.loaders.*
import com.badlogic.gdx.files.*
import com.badlogic.gdx.utils.Array
import hr.mlovrekov.thecastle.util.box2d.*

class Box2dEntityTemplateLoader(resolver: FileHandleResolver) :
        AsynchronousAssetLoader<Box2dEntityTemplate, Box2dEntityTemplateLoader.Box2dEntityTemplateParameter>(resolver) {

    private var box2dEntityTemplate: Box2dEntityTemplate? = null

    override fun loadAsync(manager: AssetManager,
                           fileName: String,
                           file: FileHandle?,
                           parameter: Box2dEntityTemplateParameter) {
        box2dEntityTemplate = parameter.box2dEntityTemplate
    }

    override fun loadSync(manager: AssetManager,
                          fileName: String,
                          file: FileHandle?,
                          parameter: Box2dEntityTemplateParameter): Box2dEntityTemplate? {
        val box2dEntityTemplate = this.box2dEntityTemplate
        this.box2dEntityTemplate = null
        return box2dEntityTemplate
    }

    override fun getDependencies(fileName: String,
                                 file: FileHandle?,
                                 parameter: Box2dEntityTemplateParameter): Array<AssetDescriptor<Any>>? {
        return null
    }

    class Box2dEntityTemplateParameter(val box2dEntityTemplate: Box2dEntityTemplate) : AssetLoaderParameters<Box2dEntityTemplate>()
}