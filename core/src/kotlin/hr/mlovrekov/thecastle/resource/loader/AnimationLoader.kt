/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.resource.loader

import com.badlogic.gdx.assets.*
import com.badlogic.gdx.assets.loaders.*
import com.badlogic.gdx.files.*
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.*
import hr.mlovrekov.thecastle.util.gdx.*
import com.badlogic.gdx.utils.Array as GdxArray

class AnimationLoader(val resolver: FileHandleResolver) : AsynchronousAssetLoader<Animation, AnimationLoader.AnimationParameter>(
        resolver) {

    companion object {
        private const val DEFAULT_FRAME_DURATION: Float = 0.5f
        private val DEFAULT_PARAMETER = AnimationParameter()
    }

    override fun loadAsync(manager: AssetManager,
                           fileName: String,
                           file: FileHandle?,
                           parameter: AnimationParameter?) {}

    override fun loadSync(manager: AssetManager,
                          fileName: String,
                          file: FileHandle?,
                          parameter: AnimationParameter?): Animation? {
        val param = parameter ?: DEFAULT_PARAMETER
        val textureName = param.textureName ?: fileName
        val spriteSheet = manager.get(textureName, Texture::class.java)

        val frames: GdxArray<TextureRegion> = GdxArray(param.repeatY * param.repeatX, { i ->
            val x = i % param.repeatX
            val y = i / param.repeatX
            TextureRegion(spriteSheet, param.startX + param.width * x, param.startY + param.height * y, param.width, param.height)
        })

        return Animation(param.frameDuration, frames)
    }

    override fun getDependencies(fileName: String,
                                 file: FileHandle?,
                                 parameter: AnimationParameter?): GdxArray<AssetDescriptor<out Any>> {
        val dependencies = GdxArray<AssetDescriptor<out Any>>(1)
        val textureName = parameter?.textureName ?: fileName
        dependencies.add(AssetDescriptor(textureName, Texture::class.java))
        return dependencies
    }

    class AnimationParameter(val textureName: String? = null,
                             val startX: Int = 0,
                             val startY: Int = 0,
                             val width: Int = 0,
                             val height: Int = 0,
                             val repeatX: Int = 0,
                             val repeatY: Int = 1,
                             val frameDuration: Float = DEFAULT_FRAME_DURATION) : AssetLoaderParameters<Animation>()
}