/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.game.strategy

import com.artemis.*
import com.artemis.utils.*
import com.badlogic.gdx.*
import hr.mlovrekov.thecastle.util.artemis.util.system.*
import hr.mlovrekov.thecastle.config.*
import hr.mlovrekov.thecastle.util.gdx.*
import com.badlogic.gdx.utils.Array as GdxArray

class FixedTimestepInvocationStrategy : SystemInvocationStrategy() {

    private val logicSystems = com.badlogic.gdx.utils.Array<BaseSystem>()
    private val otherSystems = com.badlogic.gdx.utils.Array<BaseSystem>()

    private var systemsSorted = false

    private var accumulator: Float = 0.0f

    override fun process(systems: Bag<BaseSystem>) {
        if(!systemsSorted) {
            sortSystems(systems)
        }

        world.delta = Gdx.graphics.deltaTime

        accumulator += Math.min(world.delta, MAX_STEPS * STEP_TIME)

        while(accumulator >= STEP_TIME) {
            for(system in logicSystems) {
                system.process()
                updateEntityStates()
            }
            accumulator -= STEP_TIME
        }

        world.delta = accumulator / STEP_TIME
        for(system in otherSystems) {
            system.process()
            updateEntityStates()
        }
    }

    private fun sortSystems(systems: Bag<BaseSystem>) {
        for(system in systems) {
            if(system.javaClass.isAnnotationPresent(LogicSystem::class.java)) {
                logicSystems += system
            } else {
                otherSystems += system
            }
        }
        systemsSorted = true
    }
}