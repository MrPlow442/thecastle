/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.command

import com.artemis.*
import hr.mlovrekov.thecastle.component.*
import hr.mlovrekov.thecastle.system.*

class JumpCommand(private val jumpForce: Float): Command {

    override fun invoke(world: World, entityId: Int) {
        val box2dSystem = world.getSystem(Box2dSystem::class.java)
        box2dSystem.applyForceToCenter(entityId, 0f, jumpForce, true)
    }

    override fun canInvoke(world: World, entityId: Int): Boolean {
        val box2dComponentMapper = world.getMapper(Box2dComponent::class.java)
        val box2dComponent = box2dComponentMapper[entityId]
        return box2dComponent.isGrounded && box2dComponent.linearVelocityY <= 0f
    }
}