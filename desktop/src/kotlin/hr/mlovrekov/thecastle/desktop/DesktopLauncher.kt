/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.desktop

import com.badlogic.gdx.backends.lwjgl.*
import hr.mlovrekov.thecastle.*
import hr.mlovrekov.thecastle.config.*
import hr.mlovrekov.thecastle.desktop.input.*

object DesktopLauncher {
    @JvmStatic fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration().apply {
            width = 960
            height = 640
            foregroundFPS = 60
            backgroundFPS = 60
        }

        val platformProperties = PlatformProperties(DesktopInputProcessor())
        LwjglApplication(Game(platformProperties), config)
    }
}
