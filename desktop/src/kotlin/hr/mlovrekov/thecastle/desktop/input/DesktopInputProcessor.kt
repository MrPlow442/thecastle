/*
 * Copyright 2016 mlovrekov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hr.mlovrekov.thecastle.desktop.input

import com.badlogic.gdx.*
import hr.mlovrekov.thecastle.input.*

//TODO: read key bindings from a settings file instead of hardcoding them
class DesktopInputProcessor : PlatformInputAdapter() {
    override fun keyDown(keycode: Int): Boolean {
        when (keycode) {
            Input.Keys.W      -> controllerDelegate.activate(InputController.Inputs.UP)
            Input.Keys.S      -> controllerDelegate.activate(InputController.Inputs.DOWN)
            Input.Keys.A      -> controllerDelegate.activate(InputController.Inputs.LEFT)
            Input.Keys.D      -> controllerDelegate.activate(InputController.Inputs.RIGHT)
            Input.Keys.SPACE  -> controllerDelegate.activate(InputController.Inputs.JUMP)
            Input.Keys.ENTER  -> controllerDelegate.activate(InputController.Inputs.ACCEPT)
            Input.Keys.ESCAPE -> controllerDelegate.activate(InputController.Inputs.DECLINE)
            else -> return false
        }
        return true
    }

    override fun keyUp(keycode: Int): Boolean {
        when (keycode) {
            Input.Keys.W      -> controllerDelegate.deactivate(InputController.Inputs.UP)
            Input.Keys.S      -> controllerDelegate.deactivate(InputController.Inputs.DOWN)
            Input.Keys.A      -> controllerDelegate.deactivate(InputController.Inputs.LEFT)
            Input.Keys.D      -> controllerDelegate.deactivate(InputController.Inputs.RIGHT)
            Input.Keys.SPACE  -> controllerDelegate.deactivate(InputController.Inputs.JUMP)
            Input.Keys.ENTER  -> controllerDelegate.deactivate(InputController.Inputs.ACCEPT)
            Input.Keys.ESCAPE -> controllerDelegate.deactivate(InputController.Inputs.DECLINE)
            else -> return false
        }
        return true
    }
}