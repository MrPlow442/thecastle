# README #

Supposedly a 2D sidescroller with procedurally generated maps(Think [Rogue Legacy](http://roguelegacy.com/)) only way more primitive

### Game ###

* Version: NOT.EVEN.CLOSE-TO-HAVING-A-VERSION

### How do I get set up? ###

1. Clone `git clone https://bitbucket.org/MrPlow442/thecastle.git`
2. Run `gradlew desktop:run` to run the game

### TODO list ###
- gui rework
- console
- rework sensors in box2dcomponent
- finish player camera
- map generator
- items & inventory
- enemy ai
- boss ai

### BUGS ###
- Seemingly random twitchy movement. Game seems to act different each time it's run. Probable cause: Fixed timestep implementation. NOTE: Might be fixed, needs more testing
- Screen sometimes turns completely black, only seems to occur when game window is not the main OS focus. Occurs at random.